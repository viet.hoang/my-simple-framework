﻿namespace VietHoang.Hoavi.Domain
{
    public static class Templates
    {
        public struct StandardPage
        {
            public const string Id = "{470DA31C-1F5C-4EF7-80C3-B7CF9FAEABB6}";
        }

        public struct SimplePage
        {
            public const string Id = "{78C52A8A-1FC9-4B34-BEED-A321EAAFF2B2}";
        }

        public struct Map
        {
            public const string Id = "{FB2F65F6-B46A-4327-958B-2D7CA14B0177}";

            public struct Fields
            {
                public const string MapImage = "{C2182D49-4CF9-4C5A-B5BF-790C006FB626}";
            }
        }

        public struct Location
        {
            public const string Id = "{1DF0703F-800A-4861-A04D-0C6BEBBBC3E2}";

            public struct Fields
            {
                public const string LocationName = "{AB5AB347-1ECC-42C1-BE8D-1DFD16620352}";
                public const string LocationCoordinates  = "{B5209B81-B859-4859-8C46-D5FFBB6353A6}";
            }
        }

        public struct SiteRoot
        {
            public const string Id = "{72287BBF-7540-493A-98B6-4121E16C08E2}";
        }
    }
}

﻿using VietHoang.Orm.Interfaces;

namespace VietHoang.Orm.Factories
{
    internal class MasterDatabaseContext : IDatabaseContext
    {
        public string Name => "master";
    }
}
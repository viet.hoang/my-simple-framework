﻿namespace VietHoang.Dictionary.Interfaces
{
    public interface IDictionaryEntryRepository
    {
        Models.Dictionary Dictionary { get; }

        string Get(string relativePath, string defaultValue = "");
    }
}
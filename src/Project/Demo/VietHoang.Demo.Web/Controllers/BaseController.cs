﻿using System.Web.Mvc;
using VietHoang.Demo.Services.Interfaces;
using VietHoang.Demo.Web.Utilities;
using VietHoang.Shared.Services.ObjectMapping;
using SC = Sitecore;

namespace VietHoang.Demo.Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController(IMapModelService mapModelService, IApplicationService applicationService)
        {
            MapModelService = mapModelService;
            ApplicationService = applicationService;
        }

        public IMapModelService MapModelService { get; set; }

        public IApplicationService ApplicationService { get; set; }

        protected string GetViewName(string viewPath, bool includeControllerName = false)
        {
            var controllerName = includeControllerName
                ? this.ControllerContext.RouteData.Values["controller"].ToString()
                : string.Empty;
            var actionName = this.ControllerContext.RouteData.Values["action"].ToString();
            return $"~/Views/{Shared.Constants.Global.SitecoreSolutionName}/{SC.Context.Site.Name}/{viewPath}/{controllerName}{actionName}.cshtml";
        }

        protected string GetViewName(string viewPath, string actionName)
        {
            return WebUtility.GetMvcViewName(SC.Context.Site.Name, viewPath, actionName);
        }
    }
}
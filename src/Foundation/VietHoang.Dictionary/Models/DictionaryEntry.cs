﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Dictionary.Models
{
    [SitecoreType(TemplateId = Templates.DictionaryEntry.Id, AutoMap = true)]
    public class DictionaryEntry : StandardItem
    {
        [SitecoreField(Templates.DictionaryEntry.Fields.Phrase)]
        virtual public string Phrase { get; set; }
    }
}
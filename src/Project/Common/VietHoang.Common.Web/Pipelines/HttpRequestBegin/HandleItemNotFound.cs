﻿using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Web;
using VietHoang.Shared.Extensions;
using System.Collections.Generic;
using System.Linq;
using VietHoang.Multisolution.Pipelines;
using VietHoang.Common.Web.Caching;
using VietHoang.Common.Web.Services;

namespace VietHoang.Common.Web.Pipelines.HttpRequestBegin
{
    public class HandleItemNotFound : BasePipeline<HttpRequestArgs>
    {
        private const string CacheName = "VietHoang.Common.Web.Pipelines.HttpRequestBegin.HandleItemNotFound";
        private const string CacheSizeString = "1MB";

        internal static readonly ItemNotFoundCache NotFoundCache =
               new ItemNotFoundCache(CacheName, StringUtil.ParseSizeString(CacheSizeString));

        public List<string> RelativeUrlPrefixesToIgnore { get; set; }

        public HandleItemNotFound()
        {
            RelativeUrlPrefixesToIgnore = new List<string>();
        }

        protected override void ProcessCore(HttpRequestArgs args)
        {
            var enableHandleCustomErrors = MainUtil.GetBool(Context.Site.Properties[Constants.SiteInfo.Properties.EnableHandleCustomErrors], false);
            if (!enableHandleCustomErrors)
            {
                return;
            }

            if (Context.Item != null || Context.Database == null || StartsWithPrefixToIgnore(args.Url.FilePath))
            {
                return;
            }

            var notFoundItem = NotFoundItemService.GetItemBySiteProperty(Context.Site, Constants.SiteInfo.Properties.NotFoundItem);
            if (notFoundItem != null)
            {
                Context.Item = notFoundItem;
            }
            else
            {
                Log.Warn($"[{Shared.Constants.Global.SitecoreSolutionName}] - {Context.Site.Name}'s \"not found item\" does not exist so should use the static page instead", this);
                var relativeUrl = NotFoundItemService.GetRelativeFilePathBySiteProperty(Context.Site, Constants.SiteInfo.Properties.FallbackNotFoundItem);
                var notFoundItemContent = WebUtil.ExecuteWebPage(relativeUrl);
                args.HttpContext.Response.Write(notFoundItemContent);
                args.HttpContext.Response.ContentType = "text/html";
                args.HttpContext.Response.StatusCode = 404;
                args.HttpContext.Response.TrySkipIisCustomErrors = true;
                args.HttpContext.Response.End();
            }
        }

        protected virtual bool StartsWithPrefixToIgnore(string url)
        {
            return url.HasText() && RelativeUrlPrefixesToIgnore.Any(url.StartsWith);
        }
    }
}
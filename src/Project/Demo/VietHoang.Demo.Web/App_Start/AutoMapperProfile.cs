using AutoMapper;
using VietHoang.Demo.Domain.Models.Page;
using VietHoang.Demo.Web.Models;

namespace VietHoang.Demo.Web.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public override string ProfileName => typeof(AutoMapperProfile).Assembly.FullName;

        public AutoMapperProfile()
        {
            CreateMap<IStandardPage, PageViewModel>();
        }
    }
}
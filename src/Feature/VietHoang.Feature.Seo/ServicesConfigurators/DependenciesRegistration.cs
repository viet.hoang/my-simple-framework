﻿using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using VietHoang.Feature.Seo.Repositories;

namespace VietHoang.Feature.Seo.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISitemapRepository, SitemapRepository>();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Xml;
using VietHoang.Multisolution.Caching;
using VietHoang.Shared.Extensions;

namespace VietHoang.Multisolution.Providers
{
    public class SitecoreConfigMultisolutionProvider : IMultisolutionProvider
    {
        private const string CacheSizeString = "500KB";
        private const string RootNodeName = "VietHoang.sites";
        private const string SiteNodeName = "site";
        private const string NameAttributeName = "name";

        internal static readonly SolutionSitesCache ScCache =
               new SolutionSitesCache(Constants.Cache.Name, StringUtil.ParseSizeString(CacheSizeString));

        public IEnumerable<string> GetSites()
        {
            var sites = ScCache.GetSolutionSites()?.ToList();

            if (sites == null)
            {
                sites = GetSitesFromSitecoreConfig().ToList();
                ScCache.SetSolutionSites(sites);
            }

            return sites;
        }

        private IEnumerable<string> GetSitesFromSitecoreConfig()
        {
            var rootNode = Factory.GetConfigNode(RootNodeName);

            if (rootNode == null)
            {
                yield break;
            }

            var siteNodes = XmlUtil.GetChildNodes(SiteNodeName, rootNode);

            if (siteNodes?.Any() != true)
            {
                yield break;
            }

            foreach (var siteNode in siteNodes)
            {
                var siteName = XmlUtil.GetAttribute(NameAttributeName, siteNode);

                if (siteName?.HasText() != true)
                {
                    Log.Error("Site must have name", GetType());
                    continue;
                }

                yield return siteName;
            }
        }
    }
}
var path = require("path");

var viethoangUtils = {};

viethoangUtils.getAllFilesFromProjectFolder = function (dir) {

  var filesystem = require("fs");
  var results = [];
  filesystem.readdirSync(dir).forEach(function (file) {

    file = dir + '/' + file;
    var stat = filesystem.statSync(file);

    if (stat && stat.isDirectory()) {
      var basename = path.basename(file);
      if (basename !== "bin" && basename !== "obj") {
        results = results.concat(viethoangUtils.getAllFilesFromProjectFolder(file))
      }
    } else {
      if (path.basename(file) != "z.VietHoang.SerializationSettings.config")
		results.push(file);
    }
  });

  return results;
};

module.exports = viethoangUtils;

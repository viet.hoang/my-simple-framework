﻿using Sitecore.Pipelines;
using VietHoang.Multisolution.Providers;

namespace VietHoang.Multisolution.Pipelines
{
    public abstract class BasePipeline<T> where T : PipelineArgs
    {
        protected IMultisolutionProvider Provider { get; }

        protected BasePipeline() : this(new SitecoreConfigMultisolutionProvider())
        { }

        protected BasePipeline(IMultisolutionProvider provider)
        {
            Provider = provider;
        }

        public virtual void Process(T args)
        {
            if (Provider.IsCurrentSitePossibleToProcess())
            {
                ProcessCore(args);
            }
        }

        protected abstract void ProcessCore(T args);
    }
}
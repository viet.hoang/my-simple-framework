using System;
using System.Linq;
using AutoMapper;

namespace VietHoang.Shared.Services.ObjectMapping.Implementations
{
    public static class AutoMapperFactory
    {
        private static IConfigurationProvider RegisterAutoMapper()
        {
            var assemplies =
                AppDomain.CurrentDomain.GetAssemblies()
                    .Where(w => w.FullName.StartsWith(Constants.Global.SitecoreSolutionName))
                    .SelectMany(s => s.GetTypes())
                    .ToList();

            var profiles = assemplies.Where(w => w.IsSubclassOf(typeof(Profile))).ToList();

            return new MapperConfiguration(cfg =>
            {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }
            });
        }

        private static Lazy<IConfigurationProvider> ConfigurationProvider => new Lazy<IConfigurationProvider>(RegisterAutoMapper);

        public static IMapper CreateMapper()
        {
            return ConfigurationProvider.Value.CreateMapper();
        }
    }
}
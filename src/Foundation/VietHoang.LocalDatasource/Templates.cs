﻿using Sitecore.Data;

namespace VietHoang.LocalDatasource
{
    public class Templates
    {
        public struct RenderingOptions
        {
            public static readonly ID ID = new ID("{D1592226-3898-4CE2-B190-090FD5F84A4C}");

            public struct Fields
            {
                public static readonly ID SupportsLocalDatasource = new ID("{3AE83D4F-84E9-44F6-97B2-7702059BBAE5}");
            }
        }
    }
}
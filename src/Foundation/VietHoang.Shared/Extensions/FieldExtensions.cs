﻿using System;
using Sitecore;
using Sitecore.Data.Fields;

namespace VietHoang.Shared.Extensions
{
    public static class FieldExtensions
    {
        public static bool IsChecked(this Field checkboxField)
        {
            if (checkboxField == null)
            {
                throw new ArgumentNullException(nameof(checkboxField));
            }

            return MainUtil.GetBool(checkboxField.Value, false);
        }
    }
}

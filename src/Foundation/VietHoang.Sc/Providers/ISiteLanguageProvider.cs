﻿using System.Collections.Generic;

namespace VietHoang.Sc.Providers
{
    public interface ISiteLanguageProvider
    {
        IEnumerable<Sitecore.Globalization.Language> GetSupportedLanguages();
    }
}

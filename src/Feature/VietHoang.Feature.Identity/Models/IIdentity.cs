﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;

namespace VietHoang.Feature.Identity.Models
{
    public interface IIdentity
    {
        [SitecoreField(Templates.Identity.Fields.Logo)]
        Image Logo { get; set; }

        [SitecoreField(Templates.Identity.Fields.Copyright)]
        string Copyright { get; set; }

        [SitecoreField(Templates.Identity.Fields.OrganisationName)]
        string OrganisationName { get; set; }

        [SitecoreField(Templates.Identity.Fields.OrganisationAddress)]
        string OrganisationAddress { get; set; }

        [SitecoreField(Templates.Identity.Fields.OrganisationPhone)]
        string OrganisationPhone { get; set; }

        [SitecoreField(Templates.Identity.Fields.OrganisationEmail)]
        string OrganisationEmail { get; set; }
    }
}
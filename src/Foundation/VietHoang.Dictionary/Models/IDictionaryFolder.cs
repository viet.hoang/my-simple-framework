﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Dictionary.Models
{
    [SitecoreType(TemplateId = Templates.DictionaryFolder.Id, AutoMap = true)]
    public interface IDictionaryFolder : IStandardItem, INeedsChildren
    { 
    }
}
﻿using VietHoang.Core.Models.BaseModels;
using Sitecore.Sites;

namespace VietHoang.Dictionary.Models
{
    public class Dictionary
    {
        public Dictionary(SiteContext site, IStandardItem root, bool autoCreateEntry)
        {
            Site = site;
            Root = root;
            AutoCreateEntry = autoCreateEntry;
        }

        public SiteContext Site { get; protected set; }
        public IStandardItem Root { get; protected set; }
        public bool AutoCreateEntry { get; protected set; }
    }
}
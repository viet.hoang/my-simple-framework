﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Feature.Metadata.Models
{
    [SitecoreType(TemplateId = Templates.Metadata.Id, AutoMap = true)]
    public interface IMetadata : IStandardItem
    {
        [SitecoreField(Templates.Metadata.Fields.PageTitle)]
        string PageTitle { get; set; }

        [SitecoreField(Templates.Metadata.Fields.PageDescription)]
        string PageDescription { get; set; }

        [SitecoreField(Templates.Metadata.Fields.PageKeywords)]
        string PageKeywords { get; set; }

        [SitecoreField(Templates.Metadata.Fields.MetaTitle)]
        string MetaTitle { get; set; }

        [SitecoreField(Templates.Metadata.Fields.MetaDescription)]
        string MetaDescription { get; set; }

        [SitecoreField(Templates.Metadata.Fields.MetaImage)]
        Image MetaImage { get; set; }

        [SitecoreField(Templates.Metadata.Fields.MetaType)]
        string MetaType { get; set; }

        [SitecoreField(Templates.Metadata.Fields.MetaCard)]
        string MetaCard { get; set; }

        [SitecoreField(Templates.Metadata.Fields.CanIndex)]
        bool CanIndex { get; set; }

        [SitecoreField(Templates.Metadata.Fields.SeoFollowLinks)]
        bool SeoFollowLinks { get; set; }
    }
}
﻿using VietHoang.Feature.Language.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;
using VietHoang.Sc.Providers;
using VietHoang.Feature.Language.Providers;

namespace VietHoang.Feature.Language.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ILanguageRepository, LanguageRepository>();
            serviceCollection.AddTransient<ISiteLanguageProvider, SiteLanguageProvider>();
        }
    }
}
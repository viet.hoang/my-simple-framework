﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Demo.Domain.Models.Page
{
    [SitecoreType(TemplateId = Templates.ErrorPage.Id, AutoMap = true)]
    public interface IErrorPage : IBaseContent
    {
    }
}

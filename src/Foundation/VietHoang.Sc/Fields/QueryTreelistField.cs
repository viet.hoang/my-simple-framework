﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Web.UI.HtmlControls.Data;

namespace VietHoang.Sc.Fields
{
    public class QueryTreelistField : Sitecore.Shell.Applications.ContentEditor.TreeList
    {
        private const string RootPath = "rootPath";

        private string _dataSource = string.Empty;

        public override string DataSource
        {
            get
            {
                if (_dataSource.StartsWith("query:"))
                {
                    if (Sitecore.Context.ContentDatabase == null || base.ItemID == null)
                    {
                        return null;
                    }

                    var current = Sitecore.Context.ContentDatabase.GetItem(base.ItemID);
                    Item obj = null;

                    try
                    {
                        obj = LookupSources.GetItems(current, _dataSource).FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Treelist field failed to execute query.", ex, (object)this);
                    }

                    return obj?.Paths.FullPath;
                }

                if (!_dataSource.StartsWith("$(rootpath)")) return _dataSource;
                var sites = GetSitePaths().OrderByDescending(x => x.Length);
                var item = Sitecore.Context.ContentDatabase.GetItem(ItemID);

                if (item == null) return _dataSource;
                var path = item.Paths.Path.ToLower();
                var site = sites.FirstOrDefault(x => path.StartsWith(x));

                if (site != null)
                {
                    _dataSource = _dataSource.Replace("$(rootpath)", site);
                }

                return _dataSource;
            }
            set { _dataSource = value; }
        }

        private static IEnumerable<string> GetSitePaths()
        {
            return Sitecore.Sites.SiteManager.GetSites()
                .Where(s => s.Properties.Keys.Any(x => string.Compare(x, RootPath, StringComparison.OrdinalIgnoreCase) == 0))
                .Select(s => s.Properties[RootPath].ToLower());
        }
    }
}

﻿using System.Web.Mvc;

namespace VietHoang.Shared.Extensions
{
    public static class ControllerExtensions
    {
        public static ViewResult Rendering(this Controller controller)
        {
            return controller.Rendering(null);
        }

        public static ViewResult Rendering(this Controller controller, object model)
        {
            var viewData = controller.ViewData;

            if (model != null)
            {
                viewData.Model = model;
            }

            var result = new ViewResult
            {
                ViewName = GetViewPath(controller),
                ViewData = viewData,
                TempData = controller.TempData,
                ViewEngineCollection = controller.ViewEngineCollection
            };

            return result;
        }

        private static string GetViewPath(ControllerBase controller)
        {
            var controllerName = controller.ControllerContext.RouteData.Values["controller"];
            var actionName = controller.ControllerContext.RouteData.Values["action"];
            return $"~/Views/{Constants.Global.SitecoreSolutionName}/{controllerName}/{actionName}.cshtml";
        }
    }
}

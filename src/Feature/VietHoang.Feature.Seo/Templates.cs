﻿using Sitecore.Data;

namespace VietHoang.Feature.Seo
{
    public struct Templates
    {
        public struct Sitemap
        {
            public static readonly ID Id = new ID("{DF261FBC-99C9-4373-936E-90D37EEF85E0}");

            public struct Fields
            {
                public static readonly ID AddToSitemapXml = new ID("{7A72749D-C08F-4CC9-A757-A8DDF7328304}");
                public static readonly ID IncludeChildren = new ID("{01FB6F94-DA8E-4F88-A83B-03368DF3CA3F}");
                public static readonly ID ChangeFrequency = new ID("{D2DE60F9-3110-4CE4-8296-682A4DD58014}");
                public static readonly ID Priority = new ID("{9069FB5A-9542-4701-8A74-D52324D2B523}");
            }
        }

        public struct SitemapChangeFrequency
        {
            public static readonly ID Id = new ID("{5EA0E737-4F48-410F-8C34-006A9F33A1B6}");

            public struct Fields
            {
                public static readonly ID Value = new ID("{39D0E29F-C57C-4A00-8138-EC319DE4E07D}");
            }
        }

        public struct SitemapSettings
        {
            public static readonly ID Id = new ID("{9A8762A2-C229-4822-960F-8857A21D8EC2}");

            public struct Fields
            {
                public static readonly ID DateFormat = new ID("{95EBF9B8-A988-4C84-9653-DE92D24C9208}");
                public static readonly ID DefaultChangeFrequency = new ID("{467E466F-E324-438D-864D-3894F0978C06}");
                public static readonly ID DefaultPriority = new ID("{4E95C1ED-5CB7-4C95-9CB5-4D321C6499C9}");
            }
        }
    }
}
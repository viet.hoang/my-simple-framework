﻿(function (window) {
    var c, ctx, img, r, th, ir, x, y;

    function drawMarker() {
        ctx.save();

        ctx.clearRect(0, 0, c.width, c.height);
        ctx.drawImage(img, 0, 0, c.width, c.height);

        ctx.fillStyle = "#c92a2a";
        ctx.beginPath();
        ctx.arc(x, y - th, r, -3 * Math.PI / 2, 3 * Math.PI / 2);
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x - r * 0.92, y - th * 0.8);
        ctx.lineTo(x + r * 0.92, y - th * 0.8);
        ctx.fill();

        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        ctx.arc(x, y - th, ir, 0, 2 * Math.PI);
        ctx.fill();

        ctx.restore();
    }

    function mapMarker(canvasId, imageUrl, initX, initY) {
        var self = this;

        c = document.getElementById(canvasId);

        this.onClick = null;

        img = new Image();
        img.onload = function () {
            c.width = img.width;
            c.height = img.height;

            ctx = c.getContext("2d");

            r = c.width / 100;
            th = r * 2;
            ir = r / 3;

            x = (initX || 0) * c.width;
            y = (initY || 0) * c.height;

            drawMarker();

            c.addEventListener("click", function (e) {
                var rect = c.getBoundingClientRect();
                x = e.clientX - rect.left;
                y = e.clientY - rect.top;

                drawMarker();

                if (self.onClick) {
                    self.onClick(x, y, c.width, c.height);
                }
            });
        };
        img.src = imageUrl;
    }

    window.MapMarker = mapMarker;
})(window);
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VietHoang.Feature.Metadata.Models;

namespace VietHoang.Feature.Metadata.Repositories
{
    public interface IMetadataRepository
    {
        IMetadata GetCurrentMetadata();
    }
}

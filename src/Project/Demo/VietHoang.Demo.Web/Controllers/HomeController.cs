﻿using System.Web.Mvc;
using VietHoang.Demo.Services.Interfaces;
using VietHoang.Demo.Web.Models;
using VietHoang.Shared.Services.ObjectMapping;

namespace VietHoang.Demo.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IMapModelService mapModelService, IApplicationService applicationService)
            : base(mapModelService, applicationService)
        {
        }

        public ActionResult Index()
        {
            var homepage = ApplicationService.GetHomePage();
            var model = MapModelService.Map<PageViewModel>(homepage);
            return View(GetViewName("Pages", includeControllerName: true), model);
        }
    }
}
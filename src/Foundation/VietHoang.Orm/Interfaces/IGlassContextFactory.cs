﻿using Glass.Mapper.Sc.Web;
using Glass.Mapper.Sc.Web.Mvc;

namespace VietHoang.Orm.Interfaces
{
    public interface IGlassContextFactory
    {
        IRequestContext GetRequestContext();
        IMvcContext GetMvcContext();
    }
}
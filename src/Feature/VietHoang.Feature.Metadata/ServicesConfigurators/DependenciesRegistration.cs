﻿using VietHoang.Feature.Metadata.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.DependencyInjection;

namespace VietHoang.Feature.Metadata.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IMetadataRepository, MetadataRepository>();
        }
    }
}

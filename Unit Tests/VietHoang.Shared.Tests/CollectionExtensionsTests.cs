﻿using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using VietHoang.Shared.Extensions;

namespace VietHoang.Shared.Tests
{
    [TestClass]
    public class CollectionExtensionsTests
    {
        #region HasItems Tests

        [TestMethod]
        public void HasItems_PassInNull_ReturnFalse()
        {
            // Arrange
            List<string> list = null;
            const bool expected = false;

            // Act
            var actual = list.HasItems();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasItems_PassInEmpty_ReturnFalse()
        {
            // Arrange
            var list = new List<string>();
            const bool expected = false;

            // Act
            var actual = list.HasItems();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void HasItems_PassInWithItems_ReturnTrue()
        {
            // Arrange
            var list = new List<string> { "foo bar" };
            const bool expected = true;

            // Act
            var actual = list.HasItems();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        #endregion // HasItems Tests

        #region Join Tests

        [TestMethod]
        public void Join_PassInNull_ReturnEmptyString()
        {
            // Arrange
            NameValueCollection col = null;
            var expected = string.Empty;

            // Act
            var actual = col.Join(key => string.Format("{0}={1}", key, col[key]), ",");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Join_PassInEmpty_ReturnEmptyString()
        {
            // Arrange
            var col = new NameValueCollection();
            var expected = string.Empty;

            // Act
            var actual = col.Join(key => string.Format("{0}={1}", key, col[key]), ",");

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Join_PassInCollection_ReturnCommaDelimitedString()
        {
            // Arrange
            var col = new NameValueCollection
                {
                    {
                        "firstName", "John"
                    },
                    {
                        "lastName", "Doe"
                    },
                };
            const string separator = ",";
            const string expected = "firstName=John,lastName=Doe";

            // Act
            var actual = col.Join(key => string.Format("{0}={1}", key, col[key]), separator);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Join_PassInCollection_ReturnPipeDelimitedString()
        {
            // Arrange
            var col = new NameValueCollection
                {
                    {
                        "firstName", "John"
                    },
                    {
                        "lastName", "Doe"
                    },
                };
            const string separator = "|";
            const string expected = "firstName=John|lastName=Doe";

            // Act
            var actual = col.Join(key => string.Format("{0}={1}", key, col[key]), separator);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        #endregion // Join Tests

        #region Shuffle Tests

        [TestMethod]
        public void Shuffle_PassInNull_ReturnNull()
        {
            // Arrange
            List<string> list = null;
            const object expected = null;

            // Act
            var actual = list.Shuffle();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Shuffle_PassInEmpty_ReturnEmpty()
        {
            // Arrange
            var list = new List<string>();
            const int expected = 0;

            // Act
            var actual = list.Shuffle();

            // Assert
            Assert.AreEqual(expected, actual.Count);
        }

        [TestMethod]
        public void Shuffle_PassOneItem_ReturnSame()
        {
            // Arrange
            var list = new List<string> { "item 1" };
            var expected = new List<string> { "item 1" };

            // Act
            var actual = list.Shuffle();

            // Assert
            Assert.AreEqual(expected[0], actual[0]);
        }

        [TestMethod]
        public void Shuffle_PassInTwoItems_ReturnItemsAreShuffled()
        {
            // Arrange
            var source = new List<string> { "item 1", "item 2" };

            // Act
            var result = source.Shuffle();
            var isShuffled = result.Where((t, i) => !t.Equals(source[i])).Any();

            // Assert
            Assert.IsTrue(isShuffled);
        }

        [TestMethod]
        public void Shuffle_PassInItems_ReturnItemsAreShuffled()
        {
            // Arrange
            var source = new List<string> { "item 1", "item 2", "item 3", "item 4", "item 5" };

            // Act
            var result = source.Shuffle();
            var isShuffled = result.Where((t, i) => !t.Equals(source[i])).Any();

            // Assert
            Assert.IsTrue(isShuffled);
        }

        #endregion // Shuffle Tests

        #region Paginate

        [TestMethod]
        public void Paginate_PassInPageIndexIsEqualZero_ReturnsAllItems()
        {
            // Arrange
            var enumerable = new List<int>() { 1, 2, 3, 4 }.AsEnumerable();
            const int PageIndex = 0;
            const int PageSize = 5;

            const int Expected = 4;

            // Act
            var actual = enumerable.Paginate(PageIndex, PageSize);

            // Assert
            Assert.AreEqual(Expected, actual.Count());
        }

        [TestMethod]
        public void Paginate_PassInPageIndexIsLessThanZero_ReturnsAllItems()
        {
            // Arrange
            var enumerable = new List<int>() { 1, 2, 3, 4 }.AsEnumerable();
            const int PageIndex = -1;
            const int PageSize = 5;

            const int Expected = 4;

            // Act
            var actual = enumerable.Paginate(PageIndex, PageSize);

            // Assert
            Assert.AreEqual(Expected, actual.Count());
        }

        [TestMethod]
        public void Paginate_PassInPageSizeAndPageIndex_ReturnsPagedList()
        {
            // Arrange
            var enumerable = new List<int>() { 1, 2, 3, 4 }.AsEnumerable();
            const int PageIndex = 2;
            const int PageSize = 2;

            var expected = new List<int>() { 3, 4 };

            // Act
            var actual = enumerable.Paginate(PageIndex, PageSize);

            // Assert
            Assert.AreEqual(expected.Count(), PageSize);
            CollectionAssert.AreEqual(expected, actual.ToList());
        }

        [TestMethod]
        public void Paginate_PassInPageSizeIsEqualZero_ReturnsEmptyList()
        {
            // Arrange
            var enumerable = new List<int>() { 1, 2, 3, 4 }.AsEnumerable();
            const int PageIndex = 1;
            const int PageSize = 0;

            const int Expected = 0;

            // Act
            var actual = enumerable.Paginate(PageIndex, PageSize);

            // Assert
            Assert.AreEqual(Expected, actual.Count());
        }

        [TestMethod]
        public void Paginate_PassInPageSizeIsLessThanZero_ReturnsEmptyList()
        {
            // Arrange
            var enumerable = new List<int>() { 1, 2, 3, 4 }.AsEnumerable();
            const int PageIndex = 1;
            const int PageSize = -1;

            const int Expected = 0;

            // Act
            var actual = enumerable.Paginate(PageIndex, PageSize);

            // Assert
            Assert.AreEqual(Expected, actual.Count());
        }

        #endregion
    }
}

﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Core.Models.Containers
{
    [SitecoreType(TemplateId = Templates.SettingsFolder.Id, AutoMap = true)]
    public interface ISettingsFolder : IStandardItem, INeedsChildren
    { 
    }
}
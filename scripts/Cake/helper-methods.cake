using System.Text.RegularExpressions;
using System.IO;

/*===============================================
================= HELPER METHODS ================
===============================================*/

public class Configuration
{
    private MSBuildToolVersion _msBuildToolVersion;

    public string WebsiteRoot {get;set;}
    public string InstanceUrl {get;set;}
    public string SolutionName {get;set;}
    public string ProjectFolder {get;set;}
    public string BuildConfiguration {get;set;}
    public string MessageStatisticsApiKey {get;set;}
    public string MarketingDefinitionsApiKey {get;set;}
    public bool RunCleanBuilds {get;set;}
	public string UnicornConfigurations {get;set;}

    public string BuildToolVersions 
    {
        set 
        {
            if(!Enum.TryParse(value, out this._msBuildToolVersion))
            {
                this._msBuildToolVersion = MSBuildToolVersion.Default;
            }
        }
    }

    public string SourceFolder => $"{ProjectFolder}\\src";
    public string FoundationSrcFolder => $"{SourceFolder}\\Foundation";
    public string FeatureSrcFolder => $"{SourceFolder}\\Feature";
    public string ProjectSrcFolder => $"{SourceFolder}\\Project";

    public string SolutionFile => $"{ProjectFolder}\\{SolutionName}";
    public MSBuildToolVersion MSBuildToolVersion => this._msBuildToolVersion;
    public string BuildTargets => this.RunCleanBuilds ? "Clean;Build" : "Build";
}

public void PrintHeader(ConsoleColor foregroundColor)
{
}

public void PublishProjects(string rootFolder, string websiteRoot)
{
    var projects = GetFiles($"{rootFolder}\\**\\*.csproj");

    foreach (var project in projects)
    {
        MSBuild(project, cfg => InitializeMSBuildSettings(cfg)
                                   .WithTarget(configuration.BuildTargets)
                                   .WithProperty("DeployOnBuild", "true")
                                   .WithProperty("DeployDefaultTarget", "WebPublish")
                                   .WithProperty("WebPublishMethod", "FileSystem")
                                   .WithProperty("DeleteExistingFiles", "false")
                                   .WithProperty("publishUrl", websiteRoot)
                                   .WithProperty("BuildProjectReferences", "false"));
    }
}

public FilePathCollection GetTransformFiles(string rootFolder)
{
    var xdtFiles = GetFiles($"{rootFolder}\\**\\*.xdt");
    return xdtFiles;
}

public void Transform(string rootFolder) {
    var xdtFiles = GetTransformFiles(rootFolder);

    foreach (var file in xdtFiles)
    {
        var fileFullPath = file.FullPath.ToLower();
        var buildConfiguration = configuration.BuildConfiguration.ToLower();
        if (fileFullPath.Contains($".{buildConfiguration}."))
        {
            Information($"Applying configuration transform: {fileFullPath}");
            var fileToTransform = "";
            if (fileFullPath.Contains("/app_config/"))
            {
                fileToTransform = Regex.Replace(fileFullPath, ".+/(app_config.+)/*.xdt", "$1");
            }
            else
            {
                fileToTransform = Regex.Replace(fileFullPath, ".+/(.+)/*.xdt", "$1");
            }

            fileToTransform = fileToTransform.Replace($".{buildConfiguration}.", ".");
            var sourceTransform = $"{configuration.WebsiteRoot}\\{fileToTransform}";
            Information($"The source transform: {sourceTransform}");

            XdtTransformConfig(sourceTransform			                // Source File
                                , fileFullPath			                // Tranforms file (*.xdt)
                                , sourceTransform);		                // Target File
        }
    }
}

public void RebuildIndex(string indexName)
{
    var url = $"{configuration.InstanceUrl}utilities/indexrebuild.aspx?index={indexName}";
    string responseBody = HttpGet(url);
}

public MSBuildSettings InitializeMSBuildSettings(MSBuildSettings settings)
{
    settings.SetConfiguration(configuration.BuildConfiguration)
            .SetVerbosity(Verbosity.Minimal)
            .SetMSBuildPlatform(MSBuildPlatform.Automatic)
            .SetPlatformTarget(PlatformTarget.MSIL)
            .UseToolVersion(configuration.MSBuildToolVersion)
            .WithProperty("SolutionDir", configuration.ProjectFolder)
            .WithRestore();
    return settings;
}

public void CreateFolder(string folderPath)
{
    if (!DirectoryExists(folderPath))
    {
        CreateDirectory(folderPath);
    }
}

public void WriteError(string errorMessage)
{
    cakeConsole.ForegroundColor = ConsoleColor.Red;
    cakeConsole.WriteError(errorMessage);
    cakeConsole.ResetColor();
}
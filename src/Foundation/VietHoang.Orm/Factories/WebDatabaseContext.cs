﻿using VietHoang.Orm.Interfaces;

namespace VietHoang.Orm.Factories
{
    internal class WebDatabaseContext : IDatabaseContext
    {
        public string Name => "web";
    }
}
﻿namespace VietHoang.Feature.Identity
{
    public static class Templates
    {
        public struct Identity
        {
            public const string Id = "{F7A13916-9F08-44A0-BFAC-A65B999EE0C4}";
            public struct Fields
            {
                public const string Logo = "{B5E7F5E1-87C1-40BC-973A-561177A7DF6A}";
                public const string Copyright = "{007842A1-5006-419F-BAD1-41FC186F8778}";
                public const string OrganisationName = "{F7A13916-9F08-44A0-BFAC-A65B999EE0C4}";
                public const string OrganisationAddress = "{92E04668-E00B-42D5-B3B0-ADC7FCDB720F}";
                public const string OrganisationPhone = "{8FF7BDD1-2474-45BE-BADD-DA86D59030D9}";
                public const string OrganisationEmail = "{A9E8449B-44AF-44A5-BF99-9D79B041A91E}";
                public const string SupportedLanguages = "{15D6EBDC-AA5D-4AF1-828B-1BFB8F4A9DA9}";
            }
        }
    }
}
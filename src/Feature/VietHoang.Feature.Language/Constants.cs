﻿namespace VietHoang.Feature.Language
{
    public struct Constants
    {
        public struct Dictionary
        {
            public struct Language
            {
                public static string LanguageSelectorPathFormatString => "/Languages/{0}";
            }
        }

        public struct SiteInfo
        {
            public struct Properties
            {
                public const string EnableLanguageInjector = "enableLanguageInjector";
            }
        }
    }
}

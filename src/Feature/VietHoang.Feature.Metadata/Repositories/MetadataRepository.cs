﻿using Glass.Mapper.Sc.Web;
using VietHoang.Feature.Metadata.Models;
using VietHoang.Orm.Interfaces;

namespace VietHoang.Feature.Metadata.Repositories
{
    public class MetadataRepository: IMetadataRepository
    {
        private readonly IRequestContext _requestContext;

        public MetadataRepository(IGlassContextFactory scContextFactory)
        {
            this._requestContext = scContextFactory.GetRequestContext();
        }

        public IMetadata GetCurrentMetadata()
        {
            return this._requestContext.GetContextItem<IMetadata>();
        }
    }
}

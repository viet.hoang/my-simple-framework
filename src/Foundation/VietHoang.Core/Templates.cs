﻿namespace VietHoang.Core
{
    public static class Templates
    {
        public struct StandardItem
        {
            public const string Id = "{1930BBEB-7805-471A-A3BE-4858AC7CF696}";
        }

        public struct BaseContent
        {
            public const string Id = "{CFB4EA78-857E-447B-8426-AB910D06CAA5}";

            public struct Fields
            {
                public const string Headline = "{AF322FEC-1307-4D2B-8510-B662FAE2407D}";
                public const string ShortDescription = "{84E063F7-3F26-41EE-B8B9-F5775CB24713}";
                public const string Body = "{9E4D4FD3-6D98-494A-94D7-962C43DBF12A}";
            }
        }

        public struct ContentFolder
        {
            public const string Id = "{B5E7F5E1-87C1-40BC-973A-561177A7DF6A}";
        }

        public struct OptionFolder
        {
            public const string Id = "{3A558212-F615-4BC0-996F-FF3E2DA66659}";
        }

        public struct SettingsFolder
        {
            public const string Id = "{81194B8A-DB9E-4F11-B237-DC7AE3D627EC}";
        }

        public struct BasePage
        {
            public const string Id = "{329AA89C-91E2-4E16-806C-A48A7B862AD3}";
        }

        public struct Option
        {
            public const string Id = "{33F3A764-0672-4684-839A-39C39D0C8CD9}";
            public struct Fields
            {
                public const string Value = "{FF804E78-FFF5-4F09-97C1-3C2414CE3FE9}";
            }
        }

        public struct PairSetting
        {
            public const string Id = "{8A3A476A-8EA6-47AA-A9D0-9BA43920AD9B}";
            public struct Fields
            {
                public const string Key = "{E115DB27-D956-4E82-B053-3FDD67C26421}";
                public const string Value = "{1252DE16-F198-4D01-97E6-1C7CFFEDA457}";
            }
        }
    }
}
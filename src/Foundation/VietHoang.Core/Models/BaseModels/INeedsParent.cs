﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace VietHoang.Core.Models.BaseModels
{
    [SitecoreType]
    public interface INeedsParent
    {
        [SitecoreParent(InferType = true)]
        IStandardItem Parent { get; set; }
    }
}
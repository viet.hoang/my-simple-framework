﻿using System.Text.RegularExpressions;

namespace VietHoang.Shared.Extensions
{
    public static class StringExtensions
    {
        #region HasText
        public static bool HasText(this string source)
        {
            return !string.IsNullOrWhiteSpace(source);
        }
        #endregion

        #region GetIdWithoutBraces
        public static string GetGuidWithoutBraces(this string guid)
        {
            if (string.IsNullOrWhiteSpace(guid))
                return string.Empty;
            return guid.Trim(new[] { '{', '}' });
        }
        #endregion

        #region IsNumber
        public static bool IsNumber(this string value)
        {
            if (!value.HasText())
            {
                return false;
            }

            value = value.Trim();
            return Regex.Match(value, @"^\d+$").Success;
        }
        #endregion

        #region ToInt(?int)
        public static int? ToInt(this string thisString, int? defaultValue)
        {
            if (!thisString.HasText())
                return defaultValue;

            int intValue;
            return int.TryParse(thisString, out intValue)
                    ? intValue
                    : defaultValue;
        }
        #endregion

        #region ToInt(int)
        public static int ToInt(this string thisString, int defaultValue)
        {
            if (!thisString.HasText())
                return defaultValue;

            int intValue;
            return int.TryParse(thisString, out intValue)
                    ? intValue
                    : defaultValue;
        }
        #endregion

        #region ToBool(int)
        public static bool ToBool(this string thisString, bool defaultValue = false)
        {
            if (!thisString.HasText())
                return defaultValue;

            bool boolValue;
            return bool.TryParse(thisString, out boolValue)
                    ? boolValue
                    : defaultValue;
        }
        #endregion
    }

}
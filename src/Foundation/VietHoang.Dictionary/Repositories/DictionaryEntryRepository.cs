﻿using System;
using System.Linq;
using System.Web;
using Microsoft.Extensions.DependencyInjection;
using VietHoang.Core.Models.BaseModels;
using VietHoang.Dictionary.Interfaces;
using VietHoang.Dictionary.Models;
using VietHoang.Orm.Interfaces;
using Sitecore;
using Sitecore.DependencyInjection;
using Sitecore.SecurityModel;
using Log = Sitecore.Diagnostics.Log;
using VietHoang.Shared.Extensions;
using Glass.Mapper.Sc.Web;
using Glass.Mapper.Sc;

namespace VietHoang.Dictionary.Repositories
{
    public class DictionaryEntryRepository : IDictionaryEntryRepository
    {
        private const string CacheKey = "DictionaryEntryRepository.Current";
        private readonly IRequestContext _requestContext;
        private static readonly object LockObject = new object();

        public Models.Dictionary Dictionary { get; }

        public DictionaryEntryRepository(Models.Dictionary dictionary, IGlassContextFactory sitecoreContextFactory)
        {
            Dictionary = dictionary;
            _requestContext = sitecoreContextFactory.GetRequestContext();
        }

        public static IDictionaryEntryRepository Current => GetCurrentFromCacheOrCreate();

        private static IDictionaryEntryRepository GetCurrentFromCacheOrCreate()
        {
            if (HttpContext.Current != null)
            {
                var repository =
                    HttpContext.Current.Items[CacheKey] as IDictionaryEntryRepository;

                if (repository != null)
                {
                    return repository;
                }
            }

            var glassContextFactory = ServiceLocator.ServiceProvider.GetService<IGlassContextFactory>();
            var returnValue = new DictionaryEntryRepository(DictionaryRepository.Current, glassContextFactory);

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items.Add(CacheKey, returnValue);
            }

            return returnValue;
        }

        public string Get(string relativePath, string defaultValue)
        {
            if (relativePath == null) throw new ArgumentNullException(nameof(relativePath));

            if (Context.Database == null)
            {
                return defaultValue;
            }

            var dictionaryEntry = GetOrAutoCreateDictionaryEntry(relativePath, defaultValue);

            if (dictionaryEntry == null)
            {
                return defaultValue;
            }

            return dictionaryEntry.Phrase ?? defaultValue;
        }

        private DictionaryEntry GetOrAutoCreateDictionaryEntry(string relativePath, string defaultValue)
        {
            relativePath = AssertRelativePath(relativePath);

            var dictionaryEntryPath = $"{Dictionary.Root.Path}/{relativePath}";
            var item = _requestContext.SitecoreService.GetItem<DictionaryEntry>(new Glass.Mapper.Sc.GetItemByPathOptions() { Path = dictionaryEntryPath });

            if (item != null) return item;

            if (!Dictionary.AutoCreateEntry || defaultValue == null) return null;

            try
            {
                return CreateDictionaryEntry(Dictionary, relativePath, defaultValue);
            }
            catch (Exception ex)
            {
                Log.Error($"Failed to get or create {relativePath} from the dictionary in site {Dictionary.Site.Name}",
                    ex, this);
                return null;
            }
        }

        private static string AssertRelativePath(string relativePath)
        {
            if (relativePath == null)
            {
                throw new ArgumentNullException(nameof(relativePath));
            }

            if (relativePath.StartsWith("/"))
            {
                relativePath = relativePath.Substring(1);
            }

            if (!relativePath.HasText())
            {
                throw new ArgumentException("the path is not a valid relative path", nameof(relativePath));
            }

            return relativePath;
        }

        private DictionaryEntry CreateDictionaryEntry(Models.Dictionary dictionary, string relativePath, string defaultValue)
        {
            lock (LockObject)
            {
                var parts = relativePath.Split(new[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
                var parent = dictionary.Root;

                for (var i = 0; i < parts.Length - 1; i++)
                {
                    parent = CreateDictionaryEntry(parts[i], parent);
                }

                return CreateDictionaryEntryWithPhrase(parts.Last(), defaultValue, parent);
            }
        }

        private DictionaryEntry CreateDictionaryEntry(string name, IStandardItem parent)
        {
            var dictionaryEntryPath = $"{parent.Path}/{name}";
            var item = _requestContext.SitecoreService.GetItem<DictionaryEntry>(new Glass.Mapper.Sc.GetItemByPathOptions() { Path = dictionaryEntryPath });
            if (item != null) return item;

            try
            {
                using (new SecurityDisabler())
                {
                    return _requestContext.SitecoreService.CreateItem<DictionaryEntry>(new CreateByNameOptions() { Parent = parent, Name = name });
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"Could not create item {name} under {parent.Name}", ex);
            }
        }

        private DictionaryEntry CreateDictionaryEntryWithPhrase(string name, string phrase, IStandardItem parent)
        {
            var dictionaryEntry = new DictionaryEntry
            {
                Name = name,
                Phrase = phrase
            };

            using (new SecurityDisabler())
            {
                return _requestContext.SitecoreService.CreateItem<DictionaryEntry>(new CreateByModelOptions() { Parent = parent, Model = dictionaryEntry });
            }
        }
    }
}
﻿using VietHoang.Hoavi.Domain.Models.Content;
using VietHoang.Hoavi.Domain.Models.Page;

namespace VietHoang.Hoavi.Web.Models
{
    public class PageViewModel : StandardItemViewModel, IStandardPage
    {
        public string Headline { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public ISiteRoot SiteRoot { get; set; }
    }
}
﻿using System.Collections.Generic;
using Sitecore.Data.Items;
using VietHoang.Hoavi.Domain.Models.Page;

namespace VietHoang.Hoavi.Services.Interfaces
{
    public interface IApplicationService
    {
        IStandardPage GetHomePage();
    }
}

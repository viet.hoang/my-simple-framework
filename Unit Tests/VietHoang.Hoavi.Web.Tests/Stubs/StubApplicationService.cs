﻿using System;
using System.Collections.Generic;
using Sitecore.Data.Items;
using VietHoang.Hoavi.Domain.Models.Page;
using VietHoang.Hoavi.Services.Interfaces;

namespace VietHoang.Hoavi.Web.Tests.Stubs
{
    public class StubApplicationService : IApplicationService
    {
        public IStandardPage GetHomePage()
        {
            return new Page { Id = new Guid("55db1250-bbca-44fc-be7a-c85f5ca21ec1") };
        }

        public IEnumerable<Item> GetSupportedLanguages()
        {
            throw new NotImplementedException();
        }
    }
}

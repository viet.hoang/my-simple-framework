﻿using Sitecore.Data.Items;

namespace VietHoang.Feature.Seo.Repositories
{
    public interface ISitemapRepository
    {
        string GenerateXml(Item siteRoot, string lang, int deep = -1);
        string GenerateXmlIndex(Item siteRoot);
    }
}
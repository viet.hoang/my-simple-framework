﻿using Glass.Mapper.Sc;
using VietHoang.Orm.Interfaces;
using Glass.Mapper.Sc.Web;
using Glass.Mapper.Sc.Web.Mvc;

namespace VietHoang.Orm.Factories
{
    public class GlassContextFactory : IGlassContextFactory
    {
        public GlassContextFactory()
        {
        }

        public IMvcContext GetMvcContext()
        {
            return new MvcContext();
        }

        public IRequestContext GetRequestContext()
        {
            return new RequestContext(new SitecoreService(Sitecore.Context.Database));
        }
    }
}
﻿using System;
using System.Linq;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Events;
using Sitecore.Globalization;
using VietHoang.Multisolution.Providers;
using VietHoang.Shared.Extensions;

namespace VietHoang.Common.Web.Events.ItemVersionAdded
{
    public class PrePopulateItemVersionHandler
    {
        private IMultisolutionProvider Provider { get; }

        public PrePopulateItemVersionHandler() : this(new SitecoreConfigMultisolutionProvider())
        { }

        public PrePopulateItemVersionHandler(IMultisolutionProvider provider)
        {
            Provider = provider;
        }

        public void PrePopulateItemVersion(object sender, EventArgs args)
        {
            var currentItem = Event.ExtractParameter(args, 0) as Item;
            if (currentItem == null)
            {
                return;
            }

            var siteInfo =
                Sitecore.Configuration.Factory.GetSiteInfoList()?
                    .FirstOrDefault(
                        x =>
                            x.RootPath.HasText() && x.StartItem.HasText() &&
                            !x.RootPath.Equals(Sitecore.Constants.ContentPath, StringComparison.OrdinalIgnoreCase) &&
                            currentItem.Paths.FullPath.StartsWith(x.RootPath, StringComparison.OrdinalIgnoreCase));

            if (siteInfo == null)
            {
                return;
            }

            if (Provider?.GetSites().Contains(siteInfo.Name) != true)
            {
                return;
            }

            bool enablePrePopulateItemVersion;
            bool.TryParse(siteInfo.Properties[Constants.SiteInfo.Properties.EnablePrePopulateItemVersion],
                out enablePrePopulateItemVersion);
            if (!enablePrePopulateItemVersion)
            {
                return;
            }

            Language sourceLanguage;
            Language.TryParse(siteInfo.Properties[Constants.SiteInfo.Properties.Language], out sourceLanguage);
            if (sourceLanguage == null)
            {
                return;
            }

            var currentDatabase = Sitecore.Context.ContentDatabase;
            if (currentDatabase == null)
            {
                return;
            }

            var prototypeItem = currentDatabase.GetItem(currentItem.ID, sourceLanguage).Versions.GetLatestVersion();
            if (prototypeItem == null)
            {
                Log.Error(
                    $"Error in {typeof(PrePopulateItemVersionHandler).FullName}: {siteInfo.Language} version of {currentItem.Paths.FullPath} is NOT existent.",
                    this);
                return;
            }

            prototypeItem.Fields.ReadAll();
            using (new EditContext(currentItem))
            {
                foreach (Sitecore.Data.Fields.Field field in prototypeItem.Fields)
                {
                    if (!field.Shared && !field.Name.StartsWith("__") && field.Value.HasText())
                    {
                        currentItem.Fields[field.ID].SetValue(field.Value, force: true);
                    }
                }
            }
        }
    }
}
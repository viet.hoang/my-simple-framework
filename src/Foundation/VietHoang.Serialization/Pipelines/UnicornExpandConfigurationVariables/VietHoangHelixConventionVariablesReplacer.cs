﻿using System.Collections.Generic;
using Unicorn.Pipelines.UnicornExpandConfigurationVariables;

namespace VietHoang.Serialization.Pipelines.UnicornExpandConfigurationVariables
{
    public class VietHoangHelixConventionVariablesReplacer : HelixConventionVariablesReplacer
    {
        public override Dictionary<string, string> GetVariables(string name)
        {
            var segments = name.Split('.');

            if (segments.Length < 3)
            {
                return new Dictionary<string, string>();
            }

            var vars = new Dictionary<string, string>
            {
                {"VietHoangSolution", segments[0]},
                {"VietHoangLayer", segments[1]},
                {"VietHoangModule", segments[2]}
            };

            if (segments.Length > 3)
            {
                vars.Add("VietHoangModuleConfigName", segments[3]);
            }

            return vars;
        }
    }
}

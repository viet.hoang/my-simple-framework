## HOW TO USE THE PROJECT TEMPLATES
1. copy `VietHoang.Template.Web.zip`, `VietHoang.Template.ClassLibrary.zip` files 
2. paste them to:
 * **For VS 2015**, paste them to `%USERPROFILE%\Documents\Visual Studio 2015\Templates\ProjectTemplates`
 * **For VS 2017**, paste them to `%USERPROFILE%\Documents\Visual Studio 2017\Templates\ProjectTemplates`
 * **For VS 2019**, paste them to `%USERPROFILE%\Documents\Visual Studio 2019\Templates\ProjectTemplates`

## The references:

+ https://docs.microsoft.com/en-us/nuget/visual-studio-extensibility/visual-studio-templates

+ https://msdn.microsoft.com/en-us/library/xkh1wxd8.aspx
 
+ https://msdn.microsoft.com/en-us/library/eehb4faa.aspx

## HOW TO UPDATE THE PROJECT TEMPLATES

1. open `\project_templates\VietHoang.Template\VietHoang.Template.sln`
2. update the project templates with your needs
3. open export template dialog:
 * For VS 2017 / 2019, go to **Project\Export template**.
 * For VS 2015, go to **File\Export template**.
4. choose **Project template** type then select the project you want to generate, then click **next**.
 * before clicking **Finish** button, **DO NOT FORGET** to select `Icon Image` and `Preview Image` (normally they're in `\project_templates\overridden_files`)
5. open the output location ((normally it's `C:\Users\{Username}\Documents\Visual Studio 2019\My Exported Templates`) and **UNZIP** `VietHoang.Template.Web.zip` or `VietHoang.Template.ClassLibrary.zip` to a folder
6. copy / paste all the folders and the files (in `\project_templates\overridden_files` folder) with overwrite option to the folder above then zip it

# Note for the case the package references are incorrect

- Open **Tools \ NuGet Package Manager \ Package Manager Console** then type the command: `Update-Package -reinstall -ProjectName YourProjectName`
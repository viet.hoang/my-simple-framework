﻿using Sitecore.Caching;
using System;

namespace VietHoang.Common.Web.Caching
{
    internal class ItemNotFoundCache : CustomCache
    {
        private const string ItemNotFoundCacheKey = "VietHoang.Common.Web.Caching.ItemNotFoundCache.{0}";

        public ItemNotFoundCache(string name, long maxSize)
            : base(name, maxSize)
        {
        }

        public string GetCachedNotFoundItemContent(string site)
        {
            return InnerCache.GetValue(string.Format(ItemNotFoundCacheKey, site)) as string ?? string.Empty;
        }

        public void SetNotFoundItemContent(string site, string content)
        {
            InnerCache.Add(string.Format(ItemNotFoundCacheKey, site), content, TimeSpan.FromMinutes(3));
        }
    }
}
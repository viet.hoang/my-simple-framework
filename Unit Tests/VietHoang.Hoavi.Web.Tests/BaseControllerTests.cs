﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VietHoang.Shared.Services.ObjectMapping;
using VietHoang.Shared.Services.ObjectMapping.Implementations;

namespace VietHoang.Hoavi.Web.Tests
{
    public class BaseControllerTests
    {
        protected IMapModelService MapModelService { get; set; }

        protected ControllerContext ControllerContext { get; set; }

        [TestInitialize]
        public void SetUp()
        {
            MapModelService = new AutoMapperMapModelService();

            var httpRequestMock = new Mock<HttpRequestBase>();
            httpRequestMock.Setup(r => r.HttpMethod).Returns("GET");
            var httpContextMock = new Mock<HttpContextBase>();
            httpContextMock.Setup(c => c.Request).Returns(httpRequestMock.Object);
            var routeData = new RouteData();
            routeData.Values.Add("controller", "Home");
            routeData.Values.Add("action", "Index");
            ControllerContext = new ControllerContext(httpContextMock.Object, routeData,
                new Mock<ControllerBase>().Object);
        }
    }
}

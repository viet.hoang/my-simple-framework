﻿using System.Collections.Generic;
using System.Linq;
using VietHoang.Feature.Language.Models;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using VietHoang.Orm.Interfaces;
using Glass.Mapper.Sc.Web;
using Sitecore.Links.UrlBuilders;

namespace VietHoang.Feature.Language.Repositories
{
    public class LanguageRepository : ILanguageRepository
    {
        private readonly IRequestContext _requestContext;

        public LanguageRepository(IGlassContextFactory glassContextFactory)
        {
            _requestContext = glassContextFactory.GetRequestContext();
        }

        public LanguageSelectorModel GetLanguageSelectors()
        {
            var languageSelectorModel = new LanguageSelectorModel();
            List<Item> supportedLanguageItems;

            var gmLanguages = _requestContext.GetRootItem<ILanguages>();
            supportedLanguageItems = gmLanguages.SupportedLanguages.ToList();
            if (!supportedLanguageItems.Any())
            {
                return languageSelectorModel;
            }

            var languages = Context.Database.GetLanguages().Select(language => new LanguageModel
            {
                Name = language.Name,
                NativeName = language.CultureInfo.NativeName,
                Url = GetItemUrlByLanguage(language),
                TwoLetterCode = language.CultureInfo.TwoLetterISOLanguageName
            });

            languageSelectorModel.Languages = languages.Where(language => supportedLanguageItems.Any(item => item.Name.Equals(language.Name)));
            languageSelectorModel.Current = languageSelectorModel.Languages.FirstOrDefault(x => x.Name == Context.Language.Name);

            return languageSelectorModel;
        }

        private static string GetItemUrlByLanguage(Sitecore.Globalization.Language language)
        {
            using (new LanguageSwitcher(language))
            {
                var url = LinkManager.GetItemUrl(Context.Item);
                return StringUtil.EnsurePostfix('/', url).ToLower();
            }
        }
    }
}

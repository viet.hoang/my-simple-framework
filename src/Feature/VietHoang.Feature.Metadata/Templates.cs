﻿namespace VietHoang.Feature.Metadata
{
    public static class Templates
    {
        public struct Metadata
        {
            public const string Id = "{AC9CED62-83C3-487D-8362-38DE228BAAF9}";

            public struct Fields
            {
                public const string PageTitle = "{56231F54-2712-4D6C-93B6-BEA8EF6F14E6}";
                public const string PageDescription = "{490DD3DE-3FEC-4543-9EC2-D8425E38D2AA}";
                public const string PageKeywords = "{E2EA6DFA-85EB-40EF-93D1-8AAE1E465590}";
                public const string MetaTitle = "{EF79008B-82A1-4C4A-8020-AF231407AF0E}";
                public const string MetaDescription = "{CDCF5F10-49EC-4CA2-987A-449DEA38E8B5}";
                public const string MetaImage = "{AE893AF1-1D53-42EF-A56E-443E520498E9}";
                public const string MetaType = "{41C7D91E-A871-438F-AC15-E3478EEE840D}";
                public const string MetaCard = "{427E395F-B1C7-4CE8-89D8-CB0806B7E41C}";
                public const string CanIndex = "{4AABD397-7756-4B4C-9D32-7F918076B280}";
                public const string SeoFollowLinks = "{953D8FFB-79F1-4EB8-B4C4-C2E94DB1DD30}";
            }
        }
    }
}
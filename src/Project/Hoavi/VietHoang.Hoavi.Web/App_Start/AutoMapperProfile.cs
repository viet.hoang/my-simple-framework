using AutoMapper;
using VietHoang.Hoavi.Domain.Models.Page;
using VietHoang.Hoavi.Web.Models;

namespace VietHoang.Hoavi.Web.App_Start
{
    public class AutoMapperProfile : Profile
    {
        public override string ProfileName => typeof(AutoMapperProfile).Assembly.FullName;

        public AutoMapperProfile()
        {
            CreateMap<IStandardPage, PageViewModel>();
        }
    }
}
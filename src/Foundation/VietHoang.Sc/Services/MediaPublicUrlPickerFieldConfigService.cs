﻿using SC = Sitecore;

namespace VietHoang.Sc.Services
{
    public static class MediaPublicUrlPickerFieldConfigService
    {
        public static string DialogWidth
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.MediaPublicUrlPickerField.DialogWidth", "800");

        public static string DialogHeight
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.MediaPublicUrlPickerField.DialogHeight", "150");

        public static string DomainFieldName
            =>
                SC.Configuration.Settings.GetSetting("VietHoang.Sc.MediaPublicUrlPickerField.DomainFieldName",
                    string.Empty);

        public static string DefaultPublicDomain
            =>
                SC.Configuration.Settings.GetSetting("VietHoang.Sc.MediaPublicUrlPickerField.DefaultPublicDomain",
                    string.Empty);
    }
}
﻿using System.Collections.Generic;

namespace VietHoang.Multisolution.Providers
{
    public interface IMultisolutionProvider
    {
        IEnumerable<string> GetSites();
    }
}

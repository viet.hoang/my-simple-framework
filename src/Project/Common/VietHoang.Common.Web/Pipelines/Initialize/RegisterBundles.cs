﻿using System.Web.Optimization;
using Sitecore.Pipelines;

namespace VietHoang.Common.Web.Pipelines.Initialize
{
    public class RegisterBundles
    {
        public void Process(PipelineArgs args)
        {
#if DEBUG == true
            BundleTable.EnableOptimizations = false;
#endif
        }
    }
}
﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Demo.Domain.Models.Content
{
    [SitecoreType(TemplateId = Templates.Location.Id, AutoMap = true)]
    public interface ILocation : IStandardItem
    {
        [SitecoreField(Templates.Location.Fields.LocationName)]
        string LocationName { get; set; }

        [SitecoreField(Templates.Location.Fields.LocationCoordinates)]
        string LocationCoordinates { get; set; }
    }
}

﻿using VietHoang.Feature.Language.Models;

namespace VietHoang.Feature.Language.Repositories
{
    public interface ILanguageRepository
    {
        LanguageSelectorModel GetLanguageSelectors();
    }
}
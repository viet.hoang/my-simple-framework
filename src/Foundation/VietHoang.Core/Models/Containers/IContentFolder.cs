﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Core.Models.Containers
{
    [SitecoreType(TemplateId = Templates.ContentFolder.Id, AutoMap = true)]
    public interface IContentFolder : IStandardItem, INeedsChildren
    {
    }
}
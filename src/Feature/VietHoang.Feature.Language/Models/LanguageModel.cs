﻿namespace VietHoang.Feature.Language.Models
{
    public class LanguageModel
    {
        public string NativeName { get; set; }
        public string Url { get; set; }
        public string TwoLetterCode { get; set; }
        public string Name { get; set; }
    }
}
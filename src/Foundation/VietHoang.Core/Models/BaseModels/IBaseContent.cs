﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace VietHoang.Core.Models.BaseModels
{
    [SitecoreType(TemplateId = Templates.BaseContent.Id, AutoMap = true)]
    public interface IBaseContent : IStandardItem
    {
        [SitecoreField(Templates.BaseContent.Fields.Headline)]
        string Headline { get; set; }
        [SitecoreField(Templates.BaseContent.Fields.ShortDescription)]
        string ShortDescription { get; set; }
        [SitecoreField(Templates.BaseContent.Fields.Body)]
        string Body { get; set; }
    }
}

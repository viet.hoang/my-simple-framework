﻿using System;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Links.UrlBuilders;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Sites;
using VietHoang.Multisolution.Pipelines;
using VietHoang.Sc.Providers;
using VietHoang.Shared.Extensions;

namespace VietHoang.Feature.Language.Pipelines.HttpRequestBegin
{
    public class LanguageInjector : BasePipeline<HttpRequestArgs>
    {
        private readonly ISiteLanguageProvider _siteLanguageProvider;

        public LanguageInjector(ISiteLanguageProvider siteLanguageProvider)
        {
            this._siteLanguageProvider = siteLanguageProvider;
        }

        protected override void ProcessCore(HttpRequestArgs args)
        {
            try
            {
                if (Context.Item == null || SiteContext.Current == null || args == null)
                {
                    return;
                }

                var siteContext = SiteContext.Current;

                if (siteContext.Name.Equals(Sitecore.Constants.ShellSiteName))
                {
                    return;
                }

                var currentRequest = args.HttpContext.Request;

                if (currentRequest.RawUrl.StartsWith("/sitecore", StringComparison.OrdinalIgnoreCase) ||
                    !currentRequest.HttpMethod.Equals("get", StringComparison.OrdinalIgnoreCase))
                {
                    return;
                }

                var enableLanguageInjector = MainUtil.GetBool(siteContext.Properties[Constants.SiteInfo.Properties.EnableLanguageInjector], false);

                if (!enableLanguageInjector)
                {
                    return;
                }

                if (HasLanguageInUrl(currentRequest.RawUrl, Context.Language.Name))
                {
                    return;
                }

                var currentLanguage = Context.Language;
                var supportedLanguages = this._siteLanguageProvider.GetSupportedLanguages();

                if (currentLanguage == null || supportedLanguages == null)
                {
                    return;
                }

                var userLanguage = currentRequest.UserLanguages?[0];
                userLanguage = !string.IsNullOrEmpty(userLanguage) ? userLanguage : currentLanguage.Name;

                if (!supportedLanguages.Any(x => x.Name.Equals(currentLanguage.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    Context.Language = Sitecore.Globalization.Language.Parse(supportedLanguages.FirstOrDefault()?.Name);
                }

                if (userLanguage.Length.Equals(2))
                {
                    var supportedLanguage =
                        supportedLanguages.FirstOrDefault(
                            x => x.Name.StartsWith(userLanguage, StringComparison.OrdinalIgnoreCase));
                    userLanguage = supportedLanguage != null ? supportedLanguage.Name : userLanguage;
                }

                if (supportedLanguages.Any(x => x.Name.Equals(userLanguage, StringComparison.OrdinalIgnoreCase)))
                {
                    Context.Language = Sitecore.Globalization.Language.Parse(userLanguage);
                }

                var itemLang = Context.Database.GetItem(Context.Item.ID, Context.Language);
                var url = itemLang.Url(new ItemUrlBuilderOptions
                {
                    LanguageEmbedding = LanguageEmbedding.Always,
                    LowercaseUrls = true,
                    EncodeNames = true
                });

                HttpContext.Current.Response.Redirect($"{url}{currentRequest.Url?.Query}", endResponse: false);
            }
            catch (Exception ex)
            {
                Log.Error($"Error in {typeof(LanguageInjector).FullName}: {ex.Message}", ex, this);
            }
        }

        private static bool HasLanguageInUrl(string rawUrl, string lang)
        {
            return rawUrl.StartsWith($"/{lang}", StringComparison.OrdinalIgnoreCase);
        }
    }
}
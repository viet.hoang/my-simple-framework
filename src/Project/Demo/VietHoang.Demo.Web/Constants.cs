﻿namespace VietHoang.Demo.Web
{
    public struct Constants
    {
        public struct Layouts
        {
            public const string Layout = "Layout";
            public const string OfficesMap = "OfficesMap";
        }
    }
}
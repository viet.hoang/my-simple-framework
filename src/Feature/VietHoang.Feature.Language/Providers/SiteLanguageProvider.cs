﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using VietHoang.Sc.Providers;
using VietHoang.Shared.Extensions;
using System.Collections.Generic;
using System.Linq;
using ScGlobal = Sitecore.Globalization;

namespace VietHoang.Feature.Language.Providers
{
    public class SiteLanguageProvider : ISiteLanguageProvider
    {
        private static readonly ID RegionalIsoCode = new ID("{0620F810-9294-4F14-AF9F-F5772EFCA0B2}");

        public IEnumerable<ScGlobal.Language> GetSupportedLanguages()
        {
            var settings = Context.Site.GetRootItem();
            var languageItems = settings
                .GetMultiListValueItems(Templates.Languages.Fields.SupportedLanguages.ToSitecoreId())
                .ToArray();

            if (languageItems.Any() != true)
            {
                return Enumerable.Empty<ScGlobal.Language>();
            }

            return languageItems.Select(ConvertToLanguage).Where(x => x != null);
        }

        private ScGlobal.Language ConvertToLanguage(Item languageItem)
        {
            if (languageItem?.IsDerived(TemplateIDs.Language) == true)
            {
                var iso = languageItem.Fields[RegionalIsoCode].GetValue(true);

                if (iso.HasText() && ScGlobal.Language.TryParse(iso, out var lang))
                {
                    return lang;
                }

                iso = languageItem.Fields[FieldIDs.LanguageIso].GetValue(true);

                if (ScGlobal.Language.TryParse(iso, out lang))
                {
                    return lang;
                }
            }

            return null;
        }
    }
}
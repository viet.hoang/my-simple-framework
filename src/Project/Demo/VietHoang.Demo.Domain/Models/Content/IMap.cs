﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;
using Glass.Mapper.Sc.Fields;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Demo.Domain.Models.Content
{
    [SitecoreType(TemplateId = Templates.Map.Id, AutoMap = true)]
    public interface IMap : IStandardItem
    {
        [SitecoreField(Templates.Map.Fields.MapImage)]
        Image MapImage { get; set; }

        [SitecoreChildren]
        IEnumerable<ILocation> Locations { get; set; }
    }
}

﻿namespace VietHoang.Demo.Web.Utilities
{
    public static class WebUtility
    {
        public static string GetMvcViewName(string siteName, string viewPath, string actionName)
        {
            return $"~/Views/{siteName}/{viewPath}/{actionName}.cshtml";
        }
    }
}
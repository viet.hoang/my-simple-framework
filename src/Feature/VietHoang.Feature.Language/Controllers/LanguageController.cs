﻿using System.Web.Mvc;
using Sitecore.Mvc.Controllers;
using VietHoang.Feature.Language.Repositories;
using VietHoang.Shared.Extensions;

namespace VietHoang.Feature.Language.Controllers
{
    public class LanguageController : SitecoreController
    {
        private readonly ILanguageRepository _languageRepository;

        public LanguageController(ILanguageRepository languageRepository)
        {
            this._languageRepository = languageRepository;
        }

        public ActionResult LanguageSelector()
        {
            var model = this._languageRepository.GetLanguageSelectors();
            return this.Rendering(model);
        }
}
}
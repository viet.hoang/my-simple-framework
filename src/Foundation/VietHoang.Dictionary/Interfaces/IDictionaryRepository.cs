﻿using Sitecore.Sites;

namespace VietHoang.Dictionary.Interfaces
{
    public interface IDictionaryRepository
    {
        Models.Dictionary Get(SiteContext site);
    }
}
﻿using Microsoft.Extensions.DependencyInjection;
using VietHoang.Orm.Factories;
using VietHoang.Orm.Interfaces;
using Sitecore.DependencyInjection;

namespace VietHoang.Orm.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IGlassContextFactory, GlassContextFactory>();
        }
    }
}
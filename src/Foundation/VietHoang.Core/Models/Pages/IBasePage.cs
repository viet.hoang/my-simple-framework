﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Core.Models.Pages
{
    [SitecoreType(TemplateId = Templates.BasePage.Id, AutoMap = true)]
    public interface IBasePage : IStandardItem
    { 
    }
}
﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using System.Xml;
using System.Diagnostics;
using System.Globalization;
using Sitecore.Data.Managers;
using System;
using System.Linq;
using System.Web;
using VietHoang.Sc.Providers;
using VietHoang.Shared.Extensions;
using Sitecore.Links.UrlBuilders;

namespace VietHoang.Feature.Seo.Repositories
{
    public class SitemapRepository : ISitemapRepository
    {
        private const string Xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        private ISiteLanguageProvider SiteLanguageProvider { get; }
        private string DateFormat { get; set; }
        private string ChangeFrequencyDefault { get; set; }
        private float PriorityDefault { get; set; }

        public SitemapRepository(ISiteLanguageProvider siteLanguageProvider)
        {
            var root = Sitecore.Sites.SiteContext.Current.GetRootItem();
            var dateFormatField = root[Templates.SitemapSettings.Fields.DateFormat];
            var changeFrequencyDefault = root[Templates.SitemapSettings.Fields.DefaultChangeFrequency];
            var changeFreqItem = Context.Database.GetItem(changeFrequencyDefault);
            var priorityDefault = MainUtil.GetFloat(root[Templates.SitemapSettings.Fields.DefaultPriority], 0.5f);

            this.DateFormat = !string.IsNullOrWhiteSpace(dateFormatField) ? dateFormatField : "yyyy-MM-dd";
            this.ChangeFrequencyDefault = changeFreqItem != null ? changeFreqItem[Templates.SitemapChangeFrequency.Fields.Value] : "weekly";
            this.PriorityDefault = priorityDefault;
            this.SiteLanguageProvider = siteLanguageProvider;
        }

        public string GenerateXml(Item siteRoot, string lang, int deep)
        {
            var doc = new XmlDocument();

            var declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            var urlset = doc.CreateElement("urlset", Xmlns);
            doc.AppendChild(urlset);

            if (siteRoot == null)
            {
                return doc.OuterXml;
            }

            var language = string.IsNullOrWhiteSpace(lang) ? Context.Language : LanguageManager.GetLanguage(lang);
            this.InnerAppendUrlNode(siteRoot, urlset, language, deep);
            return doc.OuterXml;
        }

        public string GenerateXmlIndex(Item siteRoot)
        {
            var doc = new XmlDocument();

            var declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            var urlset = doc.CreateElement("urlset", Xmlns);
            doc.AppendChild(urlset);

            var languages = SiteLanguageProvider.GetSupportedLanguages();
            if (languages == null || !languages.Any())
            {
                return doc.OuterXml;
            }

            var hostName = Context.Site.TargetHostName;
            var requestUrl = HttpContext.Current.Request.Url;
            if (string.IsNullOrWhiteSpace(hostName))
            {
                hostName = Context.Site.HostName;
            }

            if (string.IsNullOrWhiteSpace(hostName))
            {
                hostName = requestUrl.Host;
            }
            var lastModNodeItem = Sitecore.Sites.SiteContext.Current.GetStartItem().Axes.GetDescendants().OrderByDescending(x => x.Statistics.Updated).FirstOrDefault();
            foreach (var language in languages)
            {
                this.InnerTryMakeUrlNode($"{requestUrl.Scheme}://{hostName.TrimEnd('/')}/{language.Name.ToLower()}/sitemap.xml", urlset, lastModNodeItem?.Statistics.Updated.ToLocalTime().ToString(this.DateFormat) ?? DateTime.Now.ToLocalTime().ToString(this.DateFormat));
            }

            return doc.OuterXml;
        }

        private void InnerAppendUrlNode(Item refItem, XmlNode nodeToAppend, Sitecore.Globalization.Language language, int deep = -1)
        {
            if (refItem == null || deep == 0)
            {
                return;
            }

            foreach (Item item in refItem.Children)
            {
                var itemInSpecificLanguage = Context.Database.GetItem(item.ID, language);

                if (this.InnerTryMakeUrlNode(itemInSpecificLanguage, nodeToAppend))
                {
                    this.InnerAppendUrlNode(item, nodeToAppend, language, deep - 1);
                }
            }
        }

        private bool InnerTryMakeUrlNode(string url, XmlNode nodeToAppend, string lastModNodeItem)
        {
            Debug.Assert(nodeToAppend.OwnerDocument != null, "nodeToAppend.OwnerDocument != null");
            if (string.IsNullOrWhiteSpace(url))
            {
                return false;
            }

            var urlNode = nodeToAppend.OwnerDocument.CreateElement("url", Xmlns);
            var locNode = nodeToAppend.OwnerDocument.CreateElement("loc", Xmlns);
            var changeFreqNode = nodeToAppend.OwnerDocument.CreateElement("changefreq", Xmlns);
            var priorityNode = nodeToAppend.OwnerDocument.CreateElement("priority", Xmlns);
            var lastModNode = nodeToAppend.OwnerDocument.CreateElement("lastmod", Xmlns);

            locNode.InnerText = url;
            urlNode.AppendChild(locNode);
            changeFreqNode.InnerText = this.ChangeFrequencyDefault;
            urlNode.AppendChild(changeFreqNode);
            priorityNode.InnerText = this.PriorityDefault.ToString(CultureInfo.InvariantCulture);
            urlNode.AppendChild(priorityNode);
            lastModNode.InnerText = lastModNodeItem;
            urlNode.AppendChild(lastModNode);
            nodeToAppend.AppendChild(urlNode);

            return true;
        }

        private bool InnerTryMakeUrlNode(Item item, XmlNode nodeToAppend)
        {
            Debug.Assert(nodeToAppend.OwnerDocument != null, "nodeToAppend.OwnerDocument != null");
            if (item.Versions.Count <= 0 || !item.IsDerived(Templates.Sitemap.Id))
            {
                return false;
            }

            CheckboxField field = item.Fields[Templates.Sitemap.Fields.AddToSitemapXml];
            if (!field.Checked)
            {
                CheckboxField includeChildField = item.Fields[Templates.Sitemap.Fields.IncludeChildren];
                return includeChildField?.Checked == true;
            }

            var urlNode = nodeToAppend.OwnerDocument.CreateElement("url", Xmlns);
            var locNode = nodeToAppend.OwnerDocument.CreateElement("loc", Xmlns);
            var changeFreqNode = nodeToAppend.OwnerDocument.CreateElement("changefreq", Xmlns);
            var priorityNode = nodeToAppend.OwnerDocument.CreateElement("priority", Xmlns);
            var lastModNode = nodeToAppend.OwnerDocument.CreateElement("lastmod", Xmlns);
            var changeFreqField = item.Fields[Templates.Sitemap.Fields.ChangeFrequency];
            var priorityField = item.Fields[Templates.Sitemap.Fields.Priority];
            var options = new ItemUrlBuilderOptions
            {
                Language = item.Language,
                AlwaysIncludeServerUrl = true,
                EncodeNames = true,
                LanguageEmbedding = LanguageEmbedding.Always,
                LowercaseUrls = true
            };

            locNode.InnerText = item.Url(options);
            urlNode.AppendChild(locNode);
            if (changeFreqField.HasValue)
            {
                var changeFreqItem = Context.Database.GetItem(changeFreqField.Value);
                if (changeFreqItem != null && changeFreqItem.IsDerived(Templates.SitemapChangeFrequency.Id))
                {
                    var value = changeFreqItem.Fields[Templates.SitemapChangeFrequency.Fields.Value].Value;
                    changeFreqNode.InnerText = value;
                    urlNode.AppendChild(changeFreqNode);
                }
            }

            if (string.IsNullOrWhiteSpace(changeFreqNode.InnerText))
            {
                changeFreqNode.InnerText = this.ChangeFrequencyDefault;
                urlNode.AppendChild(changeFreqNode);
            }

            var priorityValue = MainUtil.GetFloat(priorityField.Value, this.PriorityDefault);
            if (0 <= priorityValue && priorityValue <= 1)
            {
                priorityNode.InnerText = priorityValue.ToString(CultureInfo.InvariantCulture);
                urlNode.AppendChild(priorityNode);
            }

            lastModNode.InnerText = item.Statistics.Updated.ToString(this.DateFormat);
            urlNode.AppendChild(lastModNode);
            nodeToAppend.AppendChild(urlNode);

            return true;
        }
    }
}
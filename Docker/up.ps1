$ErrorActionPreference = "Stop";

# By Jeremy Davis https://gist.github.com/jermdavis/25655ee9c095d20d15caf42fa3d27ded
function Validate-LicenseData
{
    Param (
        $EnvironmentFile = ".env",
        $EnvironmentKey = "SITECORE_LICENSE"
    )

    $file = Get-Content $EnvironmentFile -Encoding UTF8

    $key = $file | ForEach-Object {
        if($_ -imatch "^$EnvironmentKey=.*")
        {
            return $_.SubString($EnvironmentKey.Length + 1)
        }
    }
    
    $data = [System.Convert]::FromBase64String($key)

    $memory = [System.IO.MemoryStream]::new()
    $memory.Write($data, 0, $data.Length)
    $memory.Flush()   
    $memory.Seek(0, [System.IO.SeekOrigin]::Begin) | Out-Null

    $gzip = [System.IO.Compression.GZipStream]::new($memory, [System.IO.Compression.CompressionMode]::Decompress)
    
    $s = [System.IO.StreamReader]::new($gzip);
    $xml = $s.ReadToEnd()   

    $s.Dispose();
    $gzip.Dispose()
    $memory.Dispose();

    $xml -match '<expiration>(.*?)</expiration>' | Out-Null
    $textExpiry = $Matches[1]

    $expiry = [System.DateTime]::ParseExact($textExpiry, "yyyyMMddThhmmss", [System.Globalization.CultureInfo]::InvariantCulture)

    if($expiry -lt [System.DateTime]::Now)
    {
        throw "Your Sitecore license has expired."
    }
    else
    {
        $daysLeft = [int]($expiry - [System.DateTime]::Now).TotalDays
        Write-Host "You have $daysLeft days left on your license." -ForegroundColor Green
    }
}

Validate-LicenseData

# Start the Sitecore instance
Write-Host "Starting Sitecore environment..." -ForegroundColor Green
docker-compose up -d --build

# Wait for Traefik to expose CM route
Write-Host "Waiting for CM to become available..." -ForegroundColor Green
$startTime = Get-Date
do {
    Start-Sleep -Milliseconds 100
    try {
        $status = Invoke-RestMethod "http://localhost:8079/api/http/routers/cm-secure@docker"
    } catch {
        if ($_.Exception.Response.StatusCode.value__ -ne "404") {
            throw
        }
    }
} while ($status.status -ne "enabled" -and $startTime.AddSeconds(15) -gt (Get-Date))
if (-not $status.status -eq "enabled") {
    $status
    Write-Error "Timeout waiting for Sitecore CM to become available via Traefik proxy. Check CM container logs."
}

Write-Host "Opening site..." -ForegroundColor Green

$dockerContainerName = "sitecore-xp0_cm_1"
$sitecoreInstanceUrl = docker inspect --format='{{json .Config.Labels.sitecore_instance_url}}' $dockerContainerName
Start-Process $sitecoreInstanceUrl


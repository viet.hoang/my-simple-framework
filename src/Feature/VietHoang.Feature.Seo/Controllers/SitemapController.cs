﻿using System.Web.Mvc;
using VietHoang.Feature.Seo.Repositories;
using VietHoang.Shared.Extensions;

namespace VietHoang.Feature.Seo.Controllers
{
    public class SitemapController : Controller
    {
        private readonly ISitemapRepository _sitemapRepository;

        public SitemapController(ISitemapRepository sitemapRepository)
        {
            this._sitemapRepository = sitemapRepository;
        }

        public ActionResult SitemapXml(string lang)
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            var siteContext = Sitecore.Sites.SiteContextFactory.GetSiteContext(url.Host, url.PathAndQuery);
            var root = siteContext?.GetRootItem();
            var xml = lang.HasText()
                ? this._sitemapRepository.GenerateXml(root, lang)
                : this._sitemapRepository.GenerateXmlIndex(root);
            return this.Content(xml, "text/xml");
        }
    }
}

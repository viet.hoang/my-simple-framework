﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Feature.Identity.Models;
using VietHoang.Feature.Language.Models;

namespace VietHoang.Hoavi.Domain.Models.Content
{
    [SitecoreType(TemplateId = Templates.SiteRoot.Id, AutoMap = true)]
    public interface ISiteRoot : IIdentity, ILanguages
    {
    }
}

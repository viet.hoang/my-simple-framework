﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;
using VietHoang.Core.Models.Pages;
using VietHoang.Hoavi.Domain.Models.BaseModels;

namespace VietHoang.Hoavi.Domain.Models.Page
{
    [SitecoreType(TemplateId = Templates.StandardPage.Id, AutoMap = true)]
    public interface IStandardPage : IBasePage, IBaseContent, INeedsSiteRoot
    {
    }
}

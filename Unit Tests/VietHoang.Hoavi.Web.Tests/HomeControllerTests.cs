﻿using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using VietHoang.Hoavi.Services.Interfaces;
using VietHoang.Hoavi.Web.Controllers;
using VietHoang.Hoavi.Web.Models;
using Sitecore.Data;
using Sitecore.FakeDb;
using VietHoang.Hoavi.Web.Tests.Stubs;
using SC = Sitecore;

namespace VietHoang.Hoavi.Web.Tests
{
    [TestClass]
    public class HomeControllerTests : BaseControllerTests
    {
        [TestMethod]
        public void CanInitializeHomeController()
        {
            var applicationServiceMock = new Mock<IApplicationService>();
            var controller = new HomeController(MapModelService, applicationServiceMock.Object);

            Assert.IsNotNull(controller);
        }

        [TestMethod]
        public void Index_Get_ReturnHomePageViewModel()
        {
            using (var scDb = new Db())
            {
                // Arrange
                var applicationService = new StubApplicationService();
                var homepage = applicationService.GetHomePage();

                var rootItem = new DbItem("Hoavi", ID.NewID);
                var homeItem = new DbItem("Home", new ID(homepage.Id));
                rootItem.Children.Add(homeItem);
                scDb.Add(rootItem);

                var controller = new HomeController(MapModelService, applicationService)
                {
                    ControllerContext = ControllerContext
                };

                var model = controller.MapModelService.Map<PageViewModel>(homepage);

                var siteContextFake = new SC.FakeDb.Sites.FakeSiteContext(
                       new SC.Collections.StringDictionary
                       {
                        {"name", "Hoavi"},
                        {"database", "web"}
                       });

                using (new SC.Sites.SiteContextSwitcher(siteContextFake))
                {
                    // Act
                    var result = controller.Index() as ViewResult;

                    // Assert
                    Assert.IsNotNull(result);
                    Assert.IsInstanceOfType(result.Model, typeof(PageViewModel));
                    Assert.AreEqual(model.Id, ((PageViewModel)result.Model).Id);
                }
            }
        }
    }
}

﻿using System.Reflection;

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Patch Number
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyCompany("VietHoang")]
[assembly: AssemblyCopyright("Copyright © VietHoang 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.5.0")]
[assembly: AssemblyInformationalVersion("2.5.0")]
[assembly: AssemblyFileVersion("2.5.0")]
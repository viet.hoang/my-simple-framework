﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace VietHoang.Core.Models.BaseModels
{
    [SitecoreType]
    public interface INeedsChildren
    {
        [SitecoreChildren(InferType = true)]
        IEnumerable<IStandardItem> Children { get; set; }
    }
}
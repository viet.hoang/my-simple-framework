﻿using VietHoang.Demo.Domain.Models.Content;
using VietHoang.Demo.Domain.Models.Page;

namespace VietHoang.Demo.Web.Models
{
    public class PageViewModel : StandardItemViewModel, IStandardPage
    {
        public string Headline { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public ISiteRoot SiteRoot { get; set; }
    }
}
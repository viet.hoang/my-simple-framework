#addin nuget:?package=Cake.XdtTransform&version=0.17.0
#addin nuget:?package=Cake.Powershell&version=0.4.8
#addin nuget:?package=Cake.Http&version=0.7.0
#addin nuget:?package=Cake.Json&version=4.0.0
#addin nuget:?package=Newtonsoft.Json&version=12.0.3

#load "local:?path=helper-methods.cake"

var target = Argument<string>("Target", "Default");
var configuration = new Configuration();
var cakeConsole = new CakeConsole(Context.Environment);
var configJsonFile = $"../../cake-config.json";
var unicornSyncScript = $"../Unicorn/Sync.ps1";

/*===============================================
================ MAIN TASKS =====================
===============================================*/

Setup(context =>
{
    cakeConsole.ForegroundColor = ConsoleColor.Yellow;
    PrintHeader(ConsoleColor.DarkGreen);

    var configFile = new FilePath(configJsonFile);
    configuration = DeserializeJsonFromFile<Configuration>(configFile);
});


Task("Default")
.WithCriteria(configuration != null)
.IsDependentOn("Clean")
.IsDependentOn("Init-Configuration")
.IsDependentOn("Publish-All-Projects")
.IsDependentOn("Apply-Xml-Transform")
.IsDependentOn("Publish-Transforms")
.IsDependentOn("Publish-Library-Files")
.IsDependentOn("Sync-Unicorn-Root-Nodes");

Task("Deploy")
.WithCriteria(configuration != null)
.IsDependentOn("Clean")
.IsDependentOn("Init-Configuration")
.IsDependentOn("Publish-All-Projects")
.IsDependentOn("Apply-Xml-Transform")
.IsDependentOn("Publish-Transforms")
.IsDependentOn("Publish-Library-Files")
.IsDependentOn("Sync-Unicorn");

Task("Quick-Deploy")
.WithCriteria(configuration != null)
.IsDependentOn("Clean")
.IsDependentOn("Init-Configuration")
.IsDependentOn("Publish-All-Projects")
.IsDependentOn("Apply-Xml-Transform")
.IsDependentOn("Publish-Library-Files")
.IsDependentOn("Delete-Unicorn-Configs")
.IsDependentOn("Post-Deploy");

Task("Deploy-Docker")
.WithCriteria(configuration != null)
.IsDependentOn("Clean")
.IsDependentOn("Init-Configuration")
.IsDependentOn("Publish-All-Projects")
.IsDependentOn("Apply-Xml-Transform")
.IsDependentOn("Publish-Library-Files")
.IsDependentOn("Post-Deploy")
.IsDependentOn("Sync-Unicorn-Root-Nodes");

/*===============================================
================= SUB TASKS =====================
===============================================*/

Task("Clean").Does(() => {
    if (configuration.RunCleanBuilds)
    {
        Information($"Please set RunCleanBuilds to \"false\" in \"cake-config.json\" file if you don't want to perform it.");
        Information($"PLEASE CLOSE YOUR VISUAL STUDIO TO ENSURE RUNNING CLEAN BUILD SUCCESSFULY...");
        CleanDirectories($"{configuration.ProjectFolder}/**/obj");
        CleanDirectories($"{configuration.ProjectFolder}/**/bin");
        Information($"Cleaned all \"obj\" and \"bin\" folders of the solution");
    }

    var solutionConfigFolder = $"{configuration.WebsiteRoot}\\App_Config\\Include\\VietHoang";
    CleanDirectories(solutionConfigFolder);
    Information($"Cleaned the solution config folder \"{solutionConfigFolder}\"");

    var unicornFolder = $"{configuration.WebsiteRoot}\\App_Config\\Include\\Unicorn";
    CleanDirectories(unicornFolder);
    Information($"Cleaned the Unicorn folder \"{unicornFolder}\"");

    var solutionViewsFolder = $"{configuration.WebsiteRoot}\\Views\\VietHoang";
    CleanDirectories(solutionViewsFolder);
    Information($"Cleaned the solution Views folder \"{solutionViewsFolder}\"");
});

Task("Init-Configuration").Does(() => {
    var appConfigPath = $"{configuration.WebsiteRoot}\\App_Config";
    if (!FileExists(appConfigPath))
    {
        CreateFolder(appConfigPath);
    }

    Information($"Build Configuration is \"{configuration.BuildConfiguration}\"");

    var commonProjectPath = $"{configuration.ProjectFolder}\\src\\Project\\Common\\VietHoang.Common.Web";
    var configFiles = new string[] {"App_Config\\RewriteRules.config"};
    foreach(var configFile in configFiles)
    {
        Information($"Config file is \"{configFile}\"");
        var sourceFilePath = $"{commonProjectPath}\\{configFile}";
        if (!FileExists(sourceFilePath))
        {
            continue;
        }

        var destinationFilePath = $"{configuration.WebsiteRoot}\\{configFile}";
        if (!FileExists(destinationFilePath))
        {
            var filePath = new FilePath(destinationFilePath);
            Information($"Copying the config file \"{sourceFilePath}\" to \"{filePath.GetDirectory()}\"");
            CopyFileToDirectory(sourceFilePath, filePath.GetDirectory());
        }
    }
    
    configFiles = new string[] {"web.config", "App_Config\\Layers.config"};
    foreach(var configFile in configFiles)
    {
        Information($"Config file is \"{configFile}\"");
        var sourceFilePath = $"{commonProjectPath}\\{configFile}";
        if (!FileExists(sourceFilePath))
        {
            continue;
        }

        var destinationFilePath = $"{configuration.WebsiteRoot}\\{configFile}";
        if (!FileExists(destinationFilePath))
        {
            var filePath = new FilePath(destinationFilePath);
            Information($"Copying the config file \"{sourceFilePath}\" to \"{filePath.GetDirectory()}\"");
            CopyFileToDirectory(sourceFilePath, filePath.GetDirectory());
        }
    }
});

Task("Post-Deploy").Does(() => {
    var binConfigFiles = $"{configuration.WebsiteRoot}\\bin\\*.config";
    DeleteFiles(binConfigFiles);
    Information($"Deleted \"{binConfigFiles}\"");

    var binPdbFiles = $"{configuration.WebsiteRoot}\\bin\\*.pdb";
    DeleteFiles(binPdbFiles);
    Information($"Deleted \"{binPdbFiles}\"");

    var objFolder = $"{configuration.WebsiteRoot}\\obj";
    if (DirectoryExists(objFolder))
    {
        DeleteDirectory(objFolder, new DeleteDirectorySettings { Recursive = true });
        Information($"Deleted \"{objFolder}\"");
    }

    var tempFolder = $"{configuration.WebsiteRoot}\\temp";
    if (DirectoryExists(tempFolder))
    {
        DeleteDirectory(tempFolder, new DeleteDirectorySettings { Recursive = true });
        Information($"Deleted \"{tempFolder}\"");
    }
});

Task("Delete-Unicorn-Configs").Does(() => {
    var commonLocalSettingsConfig = $"{configuration.WebsiteRoot}\\App_Config\\Include\\VietHoang\\Project\\Common.LocalSettings.config";
    if (FileExists(commonLocalSettingsConfig))
    {
        DeleteFile(commonLocalSettingsConfig);
        Information($"Deleted \"{commonLocalSettingsConfig}\"");
    }

    var rainbowConfig = $"{configuration.WebsiteRoot}\\App_Config\\Include\\Rainbow.config";
    if (FileExists(rainbowConfig))
    {
        DeleteFile(rainbowConfig);
        Information($"Deleted \"{rainbowConfig}\"");
    }

    var serializationHelixConfig = $"{configuration.WebsiteRoot}\\App_Config\\Include\\VietHoang\\Foundation\\VietHoang.Serialization.Helix.config";
    if (FileExists(serializationHelixConfig))
    {
        DeleteFile(serializationHelixConfig);
        Information($"Deleted \"{serializationHelixConfig}\"");
    }

    var serializationConfigs = $"{configuration.WebsiteRoot}\\App_Config\\Include\\VietHoang\\**\\*.Serialization.config";
    DeleteFiles(serializationConfigs);
    Information($"Deleted \"{serializationConfigs}\"");

    var unicornFolder = $"{configuration.WebsiteRoot}\\App_Config\\Include\\Unicorn";
    if (DirectoryExists(unicornFolder))
    {
        DeleteDirectory(unicornFolder, new DeleteDirectorySettings { Recursive = true });
        Information($"Deleted \"{unicornFolder}\"");
    }
});

Task("Publish-Library-Files").Does(() => {

    Information($"*** Publish library files which are not automatically referenced by the build system but are used by Sitecore ***");

    var folderName = $"bin";
    var parentSourceFolder = $"{configuration.ProjectFolder}\\lib\\Sitecore";
    Information($"Copying the \"{folderName}\" folder from \"{parentSourceFolder}\" to \"{configuration.WebsiteRoot}\"");
    CopyDirectory($"{parentSourceFolder}\\{folderName}", $"{configuration.WebsiteRoot}\\{folderName}");
});

Task("Publish-All-Projects")
.IsDependentOn("Build-Solution")
.IsDependentOn("Publish-Foundation-Projects")
.IsDependentOn("Publish-Feature-Projects")
.IsDependentOn("Publish-Project-Projects");


Task("Build-Solution").Does(() => {
    MSBuild(configuration.SolutionFile, cfg => InitializeMSBuildSettings(cfg));
});

Task("Publish-Foundation-Projects").Does(() => {
    PublishProjects(configuration.FoundationSrcFolder, configuration.WebsiteRoot);
});

Task("Publish-Feature-Projects").Does(() => {
    PublishProjects(configuration.FeatureSrcFolder, configuration.WebsiteRoot);
});

Task("Publish-Project-Projects").Does(() => {
    var common = $"{configuration.ProjectSrcFolder}\\Common";
    var demo = $"{configuration.ProjectSrcFolder}\\Demo";
    var hoavi = $"{configuration.ProjectSrcFolder}\\Hoavi";

    PublishProjects(common, configuration.WebsiteRoot);
    PublishProjects(demo, configuration.WebsiteRoot);
    PublishProjects(hoavi, configuration.WebsiteRoot);
});

Task("Apply-Xml-Transform").Does(() => {
    var layers = new string[] { configuration.FoundationSrcFolder, configuration.FeatureSrcFolder, configuration.ProjectSrcFolder};

    foreach(var layer in layers)
    {
        Transform(layer);
    }
});

Task("Publish-Transforms").Does(() => {
    var layers = new string[] { configuration.FoundationSrcFolder, configuration.FeatureSrcFolder, configuration.ProjectSrcFolder};
    var destination = $@"{configuration.WebsiteRoot}\temp\transforms";

    CreateFolder(destination);

    try
    {
        var files = new List<string>();
        foreach(var layer in layers)
        {
            var xdtFiles = GetTransformFiles(layer).Select(x => x.FullPath).ToList();
            files.AddRange(xdtFiles);
        }   

        CopyFiles(files, destination, preserveFolderStructure: true);
    }
    catch (System.Exception ex)
    {
        WriteError(ex.Message);
    }
});

Task("Sync-Unicorn-Root-Nodes").Does(() => {
    var unicornUrl = configuration.InstanceUrl + "unicorn.aspx";
    Information("Sync Unicorn root items from url: " + unicornUrl);

    var authenticationFile = new FilePath($"{configuration.WebsiteRoot}/App_config/Include/Unicorn/Unicorn.zSharedSecret.config");
    var xPath = "/configuration/sitecore/unicorn/authenticationProvider/SharedSecret";

    string sharedSecret = XmlPeek(authenticationFile, xPath);
                                            
    StartPowershellFile(unicornSyncScript, new PowershellSettings()
                                                        .SetFormatOutput()
                                                        .SetLogOutput()
                                                        .WithArguments(args => {
                                                            args.Append("secret", sharedSecret)
                                                                .Append("url", unicornUrl)
                                                                .Append("configurations", configuration.UnicornConfigurations);
                                                        }));
});

Task("Sync-Unicorn").Does(() => {
    var unicornUrl = configuration.InstanceUrl + "unicorn.aspx";
    Information("Sync Unicorn root items from url: " + unicornUrl);

    var authenticationFile = new FilePath($"{configuration.WebsiteRoot}/App_config/Include/Unicorn/Unicorn.zSharedSecret.config");
    var xPath = "/configuration/sitecore/unicorn/authenticationProvider/SharedSecret";

    string sharedSecret = XmlPeek(authenticationFile, xPath);
                                            
    StartPowershellFile(unicornSyncScript, new PowershellSettings()
                                                        .SetFormatOutput()
                                                        .SetLogOutput()
                                                        .WithArguments(args => {
                                                            args.Append("secret", sharedSecret)
                                                                .Append("url", unicornUrl);
                                                        }));
});

RunTarget(target);
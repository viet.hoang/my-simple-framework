﻿using SC = Sitecore;

namespace VietHoang.Sc.Services
{
    public static class ImageCoordinatesPickerFieldConfigService
    {
        public static string DialogWidth
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.ImageCoordinatesPickerField.DialogWidth", "800");

        public static string DialogHeight
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.ImageCoordinatesPickerField.DialogHeight", "600");

        public static string ImageFieldName
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.ImageCoordinatesPickerField.ImageFieldName", "Image");

        public static string ImageAlternateText
            => SC.Configuration.Settings.GetSetting("VietHoang.Sc.ImageCoordinatesPickerField.ImageAlternateText", "There is NO image in the parent item.");
    }
}
﻿using System.Collections.Generic;

namespace VietHoang.Feature.Language.Models
{
    public class LanguageSelectorModel
    {
        public LanguageModel Current { get; set; }

        public IEnumerable<LanguageModel> Languages { get; set; }
    }
}
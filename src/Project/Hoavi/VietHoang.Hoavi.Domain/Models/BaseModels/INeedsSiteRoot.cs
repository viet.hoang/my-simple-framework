﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Hoavi.Domain.Models.Content;

namespace VietHoang.Hoavi.Domain.Models.BaseModels
{
    [SitecoreType]
    public interface INeedsSiteRoot
    {
        [SitecoreQuery("./ancestor::*[@@templateid='" + Templates.SiteRoot.Id + "']", IsRelative = true)]
        ISiteRoot SiteRoot { get; set; }
    }
}

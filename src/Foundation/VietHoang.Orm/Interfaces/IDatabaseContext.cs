﻿namespace VietHoang.Orm.Interfaces
{
    public interface IDatabaseContext
    {
        string Name { get; }
    }
}
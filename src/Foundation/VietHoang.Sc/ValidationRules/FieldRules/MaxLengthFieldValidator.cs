﻿using System;
using System.Runtime.Serialization;
using Sitecore;
using Sitecore.Data.Validators;
using VietHoang.Shared.Extensions;

namespace VietHoang.Sc.ValidationRules.FieldRules
{
    [Serializable]
    public class MaxLengthFieldValidator : StandardValidator
    {
        public override string Name => "Max Length";

        public MaxLengthFieldValidator()
        {
        }

        public MaxLengthFieldValidator(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected override ValidatorResult Evaluate()
        {
            var result = base.Parameters["Result"];
            var validatorResult = result != null
                ? (ValidatorResult) Enum.Parse(typeof (ValidatorResult), result)
                : ValidatorResult.FatalError;
            validatorResult = validatorResult == ValidatorResult.Unknown ? validatorResult : ValidatorResult.FatalError;
            var @int = MainUtil.GetInt(Parameters["maxlength"], 0);
            if (@int <= 0)
            {
                return ValidatorResult.Valid;
            }

            var controlValidationValue = base.ControlValidationValue;
            if (!controlValidationValue.HasText())
            {
                return ValidatorResult.Valid;
            }

            if (controlValidationValue.Length <= @int)
            {
                return ValidatorResult.Valid;
            }

            Text = GetText("The maximum length of the field \"{0}\" is {1} characters.", GetFieldDisplayName(),
                @int.ToString());
            return GetFailedResult(validatorResult);
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.FatalError);
        }
    }
}
﻿using System.Linq;
using Sitecore;

namespace VietHoang.Multisolution.Providers
{
    public static class MultisolutionProviderExtensions
    {
        public static bool IsCurrentSitePossibleToProcess(this IMultisolutionProvider provider)
        {
            return provider?.GetSites()?.Contains(Context.Site.Name) == true;
        }
    }
}

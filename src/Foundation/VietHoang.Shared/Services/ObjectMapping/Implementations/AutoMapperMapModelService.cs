﻿using AutoMapper;

namespace VietHoang.Shared.Services.ObjectMapping.Implementations
{
    public class AutoMapperMapModelService : IMapModelService
    {
        private readonly IMapper _mapper = AutoMapperFactory.CreateMapper();

        public TDest Map<TSrc, TDest>(TSrc source) where TDest : class
        {
            return _mapper.Map<TSrc, TDest>(source);
        }

        public TDest Map<TDest>(object source) where TDest : class
        {
            return _mapper.Map<TDest>(source);
        }
    }
}

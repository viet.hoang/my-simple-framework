﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using VietHoang.Ioc;
using Sitecore.DependencyInjection;
using VietHoang.Shared.Services.ObjectMapping;
using VietHoang.Shared.Services.ObjectMapping.Implementations;
using System.Collections.Generic;
using Sitecore.Diagnostics;

namespace VietHoang.Common.Web.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        public void Configure(IServiceCollection serviceCollection)
        {
            var assemplies = AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.GlobalAssemblyCache)
                                            .Where(w => w.FullName.StartsWith(Shared.Constants.Global.SitecoreSolutionName));

            var assemblyTypes = new List<Type>();
            foreach (var assembly in assemplies)
            {
                try
                {
                    var types = assembly.GetTypes();
                    assemblyTypes.AddRange(types);
                }
                catch(Exception ex)
                {
                    Log.Warn(ex.Message, ex, this);
                }
            }

            var servicesConfigurators =
                assemblyTypes.Where(
                    w =>
                        typeof(IServicesConfigurator).IsAssignableFrom(w) &&
                        w.FullName != typeof(DependenciesRegistration).FullName).ToList();

            foreach (var servicesConfigurator in servicesConfigurators)
            {
                var method = servicesConfigurator.GetMethod("Configure", new[] { typeof(IServiceCollection) });

                method?.Invoke(Activator.CreateInstance(servicesConfigurator), new object[] { serviceCollection });
                serviceCollection.AddMvcControllers(new Assembly[] { servicesConfigurator.Assembly });
            }

            serviceCollection.AddTransient<IMapModelService>(sp => new AutoMapperMapModelService());
        }
    }
}

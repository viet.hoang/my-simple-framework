using System.Collections.Generic;
using VietHoang.Hoavi.Domain.Models.Page;
using VietHoang.Hoavi.Services.Abstractions;
using VietHoang.Hoavi.Services.Interfaces;
using VietHoang.Orm.Interfaces;

namespace VietHoang.Hoavi.Services
{
    public class ApplicationService : BaseService, IApplicationService
    {
        public ApplicationService(IGlassContextFactory glassContextFactory)
            : base(glassContextFactory)
        {
        }

        public IStandardPage GetHomePage()
        {
            return ScRequestContext.GetHomeItem<IStandardPage>();
        }
    }
}

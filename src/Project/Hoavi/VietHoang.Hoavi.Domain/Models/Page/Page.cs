﻿using System;
using Sitecore.Data;
using Sitecore.Globalization;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Hoavi.Domain.Models.Page
{
    public class Page : IStandardPage
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        public string Path { get; set; }
        public string Url { get; set; }
        public string FullPath { get; set; }
        public string FullUrl { get; set; }
        public string Name { get; set; }
        public Guid TemplateId { get; set; }
        public string TemplateName { get; set; }
        string IStandardItem.SortOrder { get; set; }
        public int SortOrder { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Language Language { get; set; }
        public ItemUri Uri { get; set; }
        public string DisplayName { get; set; }
        public string Headline { get; set; }
        public string ShortDescription { get; set; }
        public string Body { get; set; }
        public Content.ISiteRoot SiteRoot { get; set; }
    }
}

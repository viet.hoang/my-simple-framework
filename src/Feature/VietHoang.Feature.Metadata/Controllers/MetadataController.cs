﻿using System.Web.Mvc;
using Sitecore.Mvc.Controllers;
using VietHoang.Feature.Metadata.Repositories;
using VietHoang.Shared.Extensions;

namespace VietHoang.Feature.Metadata.Controllers
{
    public class MetadataController : SitecoreController
    {
        private readonly IMetadataRepository _metadataRepository;

        public MetadataController(IMetadataRepository metadataRepository)
        {
            this._metadataRepository = metadataRepository;
        }

        public ActionResult Metadata()
        {
            var model = this._metadataRepository.GetCurrentMetadata();

            return this.Rendering(model);
        }
    }
}

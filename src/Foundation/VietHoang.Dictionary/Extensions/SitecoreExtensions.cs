﻿using VietHoang.Dictionary.Repositories;
using Sitecore.Mvc.Helpers;

namespace VietHoang.Dictionary.Extensions
{
    public static class SitecoreExtensions
    {
        public static string Dictionary(this SitecoreHelper helper, string relativePath, string defaultValue = "")
        {
            return DictionaryEntryRepository.Current.Get(relativePath, defaultValue);
        }
    }
}
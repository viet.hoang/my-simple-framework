﻿namespace VietHoang.Dictionary
{
    public static class Templates
    {
        public struct DictionaryFolder
        {
            public const string Id = "{843C8898-6811-4142-9E00-3EA6563186C7}";
        }

        public struct DictionaryEntry
        {
            public const string Id = "{C28662B8-C009-4A6E-95C2-04F5C8CF2DF1}";
            public struct Fields
            {
                public const string Phrase = "{FFAAC6F3-7DB9-495B-AB5E-819242E08C66}";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Sitecore.Caching;

namespace VietHoang.Multisolution.Caching
{
    internal class SolutionSitesCache : CustomCache
    {
        public SolutionSitesCache(string name, long maxSize) : base(name, maxSize)
        {
        }

        public object Get(string cacheKey)
        {
            var key = GetKey(cacheKey);
            return this.GetObject(key);
        }

        public void Set(string cacheKey, object value)
        {
            var key = GetKey(cacheKey);
            this.SetObject(key, value);
        }

        private static string GetKey(string key)
        {
            return key;
        }

        public IEnumerable<string> GetSolutionSites()
        {
            return InnerCache.GetValue(Constants.Cache.SolutionSitesCacheKey) as IEnumerable<string>;
        }

        public void SetSolutionSites(IEnumerable<string> sites)
        {
            InnerCache.Add(Constants.Cache.SolutionSitesCacheKey, sites, TimeSpan.FromMinutes(3));
        }
    }
}
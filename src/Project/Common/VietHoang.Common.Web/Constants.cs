﻿namespace VietHoang.Common.Web
{
    public struct Constants
    {
        public struct SiteInfo
        {
            public struct Properties
            {
                public const string Language = "language";
                public const string EnablePrePopulateItemVersion = "enablePrePopulateItemVersion";
                public const string NotFoundItem = "notFoundItem";
                public const string EnableLanguageInjector = "enableLanguageInjector";
                public const string EnableHandleCustomErrors = "enableHandleCustomErrors";
                public const string FallbackNotFoundItem = "fallbackNotFoundItem";
                public const string CustomErrorFile = "customErrorFile";
            }
        }
    }
}
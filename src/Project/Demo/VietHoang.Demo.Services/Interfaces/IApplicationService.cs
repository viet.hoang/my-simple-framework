﻿using System.Collections.Generic;
using Sitecore.Data.Items;
using VietHoang.Demo.Domain.Models.Page;

namespace VietHoang.Demo.Services.Interfaces
{
    public interface IApplicationService
    {
        IStandardPage GetHomePage();
    }
}

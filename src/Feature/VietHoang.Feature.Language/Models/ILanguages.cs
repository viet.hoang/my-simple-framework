﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System.Collections.Generic;
using VietHoang.Core.Models.BaseModels;
using Sitecore.Data.Items;

namespace VietHoang.Feature.Language.Models
{
    [SitecoreType(TemplateId = Templates.Languages.Id, AutoMap = true)]
    public interface ILanguages : IStandardItem
    {
        [SitecoreField(Templates.Languages.Fields.SupportedLanguages)]
        IEnumerable<Item> SupportedLanguages { get; set; }
    }
}
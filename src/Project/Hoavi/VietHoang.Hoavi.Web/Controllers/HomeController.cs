﻿using System.Web.Mvc;
using VietHoang.Shared.Services.ObjectMapping;
using VietHoang.Hoavi.Services.Interfaces;
using VietHoang.Hoavi.Web.Models;

namespace VietHoang.Hoavi.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IMapModelService mapModelService, IApplicationService applicationService)
            : base(mapModelService, applicationService)
        {
        }

        public ActionResult Index()
        {
            var homepage = ApplicationService.GetHomePage();
            var model = MapModelService.Map<PageViewModel>(homepage);
            return View(GetViewName("Pages", includeControllerName: true), model);
        }
    }
}
﻿using Glass.Mapper.Sc.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Sitecore.Data;
using VietHoang.Hoavi.Domain.Models.Page;
using VietHoang.Orm.Interfaces;
using SC = Sitecore;

namespace VietHoang.Hoavi.Services.Tests
{
    [TestClass]
    public class ApplicationServiceTests
    {
        [TestMethod]
        public void GetHomePage_ReturnHomePage()
        {
            // Arrange
            var homepageIdMock = ID.NewID;

            var siteContextFake = new SC.FakeDb.Sites.FakeSiteContext(
                new SC.Collections.StringDictionary
                {
                    {"name", "Hoavi"},
                    {"database", "web"}
                });

            var homePage = new Page { Id = homepageIdMock.Guid };

            var scContextMock = new Mock<IRequestContext>();
            scContextMock.Setup(x => x.GetHomeItem<IStandardPage>()).Returns(homePage);

            var glassContextFactoryMock=new Mock<IGlassContextFactory>();
            glassContextFactoryMock.Setup(x => x.GetRequestContext()).Returns(scContextMock.Object);

            var applicationPageService = new ApplicationService(glassContextFactoryMock.Object);

            using (new SC.Sites.SiteContextSwitcher(siteContextFake))
            {
                // Act
                var result = applicationPageService.GetHomePage();

                // Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(homePage, result);
            }
        }
    }
}

﻿using System;
using System.Xml;
using Sitecore.Links;
using Sitecore.Resources.Media;

namespace VietHoang.Shared.Extensions
{
    public static class XmlDocumentExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xd"></param>
        /// <param name="linkXml"></param>
        /// <returns>a pair value of link url and link text</returns>
        public static ScLink GetScLink(this XmlDocument xd, string linkXml)
        {
            if (xd == null || !linkXml.HasText()) return new ScLink();

            // Ex1: <link text="" anchor="" linktype="internal" class="" title="" target="" querystring="" id="{A0B579BF-4A2C-48EE-92E0-4C36DFA93767}" />
            // Ex2: <link linktype="external" url="https://external.com/" anchor="" target="" />
            xd.LoadXml(linkXml);

            var linkXe = xd["link"];
            if (linkXe == null) return new ScLink();

            var linkUrl = linkXe.Attributes["url"]?.Value;
            if (!linkUrl.HasText())
            {
                var itemId = linkXe.Attributes["id"]?.Value;
                var item = Sitecore.Context.Database.Items.GetItem(new Sitecore.Data.ID(Sitecore.MainUtil.GetGuid(itemId)));
                if (item != null)
                {
                    linkUrl = !item.Paths.IsMediaItem ? LinkManager.GetItemUrl(item) : MediaManager.GetMediaUrl(item);
                }
            }

            var linkText = linkXe.Attributes["text"]?.Value;
            var secureAttributes = (linkXe.Attributes["linktype"]?.Value).Equals("external", StringComparison.OrdinalIgnoreCase)
                ? "target='blank' rel='noopener noreferrer'" : string.Empty;

            return new ScLink { LinkUrl = linkUrl, LinkText = linkText, SecureAttributes = secureAttributes };
        }

        public static string GetImageSource(this XmlDocument xd, string imageXml)
        {
            var imageSource = string.Empty;
            if (xd == null || !imageXml.HasText())
            {
                return imageSource;
            }

            // Ex: "<image mediaid=\"{FD048D64-39BD-44B5-905C-3C6B74085AD0}\" />"
            xd.LoadXml(imageXml);
            var imageId = xd["image"].Attributes["mediaid"]?.Value;
            if (imageId.HasText())
            {
                var mediaItem = Sitecore.Context.Database.Items.GetItem(imageId);
                if (mediaItem != null)
                {
                    imageSource = MediaManager.GetMediaUrl(mediaItem);
                }
            }

            return imageSource;
        }

        public static string GetFileSource(this XmlDocument xd, string fileXml)
        {
            var fileSource = string.Empty;
            if (xd == null || !fileXml.HasText())
            {
                return fileSource;
            }

            // Ex: <file mediaid="{15978A17-9613-43E6-9201-5FD51C3C9872}" src="-/media/15978A17961343E692015FD51C3C9872.ashx" />
            xd.LoadXml(fileXml);
            var imageId = xd["file"].Attributes["mediaid"]?.Value;
            if (imageId.HasText())
            {
                var mediaItem = Sitecore.Context.Database.Items.GetItem(imageId);
                if (mediaItem != null)
                {
                    fileSource = MediaManager.GetMediaUrl(mediaItem);
                }
            }

            return fileSource;
        }
    }

    public class ScLink
    {
        public ScLink()
        {
            LinkUrl = string.Empty;
            LinkText = string.Empty;
            SecureAttributes = string.Empty;
        }

        public string LinkUrl { get; set; }
        public string LinkText { get; set; }
        public string SecureAttributes { get; set; }
    }
}

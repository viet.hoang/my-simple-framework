﻿using System.Collections.Generic;
using VietHoang.Orm.Enums;
using VietHoang.Orm.Interfaces;

namespace VietHoang.Orm.Factories
{
    public static class DatabaseContextCreator
    {
        private static Dictionary<DatabaseContext, IDatabaseContext> _databaseContextDictionary;

        private static Dictionary<DatabaseContext, IDatabaseContext> DatabaseContextDictionary
        {
            get
            {
                if (_databaseContextDictionary != null) return _databaseContextDictionary;

                _databaseContextDictionary = new Dictionary<DatabaseContext, IDatabaseContext>
                {
                    {DatabaseContext.Master, new MasterDatabaseContext()},
                    {DatabaseContext.Web, new WebDatabaseContext()}
                };

                return _databaseContextDictionary;
            }
        }

        public static IDatabaseContext CreateDatabaseContext(DatabaseContext context)
        {
            return DatabaseContextDictionary[context];
        }
    }
}
﻿using System;
using SC = Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Resources.Media;
using Sitecore.Web;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using VietHoang.Sc.Services;
using ImageField = Sitecore.Data.Fields.ImageField;
using Sitecore.Links.UrlBuilders;

namespace VietHoang.Sc.Dialogs
{
    public class ImageCoordinatesPickerDialog : DialogForm
    {
        private const string Separator = "|";

        private readonly Database masterDb = Factory.GetDatabase("master");

        public SC.Web.UI.HtmlControls.Image ImageFrameAltText
        { get; set; }

        public SC.Web.UI.HtmlControls.Edit TextBoxCoordinate
        { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (this.TextBoxCoordinate == null || ImageFrameAltText == null)
            {
                return;
            }

            this.TextBoxCoordinate.Value = WebUtil.GetQueryString(Constants.QueryStringKeys.Value);
            var containerId = WebUtil.GetQueryString(Constants.QueryStringKeys.ContainerId);
            var currentItem = this.masterDb.Items.GetItem(containerId);
            var parentItem = currentItem.Parent;
            var imageFieldNames = ImageCoordinatesPickerFieldConfigService.ImageFieldName.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);

            ImageField imageField = null;
            foreach (var imageFieldName in imageFieldNames)
            {
                imageField = parentItem.Fields[imageFieldName];
                if (imageField != null) break;
            }

            if (string.IsNullOrWhiteSpace(imageField?.Value))
            {
                ImageFrameAltText.Alt = ImageCoordinatesPickerFieldConfigService.ImageAlternateText;
                ImageFrameAltText.Src = "#";
                return;
            }

            var mediaItem = this.masterDb.Items.GetItem(imageField.MediaID);
            if (mediaItem == null)
            {
                ImageFrameAltText.Alt = ImageCoordinatesPickerFieldConfigService.ImageAlternateText;
                ImageFrameAltText.Src = "#";
                return;
            }

            var imageSrc = MediaManager.GetMediaUrl(
                mediaItem,
                new MediaUrlBuilderOptions
                {
                    Database = this.masterDb,
                    DisableMediaCache = true,
                    DisableBrowserCache = true,
                    AllowStretch = false
                });

            if (!string.IsNullOrWhiteSpace(imageSrc))
            {
                imageSrc += "&usecustomfunctions=1&centercrop=1";
            }

            this.TextBoxCoordinate.Attributes["data-url"] = imageSrc;
            ImageFrameAltText.Visible = false;
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            SheerResponse.SetDialogValue(System.Web.HttpContext.Current.Request.Form["TextBoxCoordinate"]);
            base.OnOK(sender, args);
        }
    }
}
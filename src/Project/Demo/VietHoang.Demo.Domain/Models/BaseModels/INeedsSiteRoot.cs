﻿using Glass.Mapper.Sc.Configuration.Attributes;

namespace VietHoang.Demo.Domain.Models.BaseModels
{
    [SitecoreType]
    public interface INeedsSiteRoot
    {
        [SitecoreQuery("./ancestor::*[@@templateid='" + Templates.SiteRoot.Id + "']", IsRelative = true)]
        Content.ISiteRoot SiteRoot { get; set; }
    }
}

using Glass.Mapper.Sc.Web;
using Glass.Mapper.Sc.Web.Mvc;
using VietHoang.Orm.Interfaces;

namespace VietHoang.Demo.Services.Abstractions
{
    public abstract class BaseService
    {
        private readonly IGlassContextFactory _glassContextFactory;

        protected BaseService(IGlassContextFactory glassContextFactory)
        {
            _glassContextFactory = glassContextFactory;
        }

        protected IRequestContext ScRequestContext => _glassContextFactory.GetRequestContext();

        protected IMvcContext ScMvcContext => _glassContextFactory.GetMvcContext();
    }
}

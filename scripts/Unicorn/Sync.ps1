param([string]$url, [string]$secret, [string[]]$configurations)
$ErrorActionPreference = 'Stop'

$ScriptPath = Split-Path $MyInvocation.MyCommand.Path

# This is an example PowerShell script that will remotely execute a Unicorn sync using the new CHAP authentication system.

Import-Module $ScriptPath\Unicorn.psm1

Sync-Unicorn -ControlPanelUrl $url -SharedSecret $secret -Configurations $configurations 

# Note: you may pass -Verb 'Reserialize' for remote reserialize. Usually not needed though.
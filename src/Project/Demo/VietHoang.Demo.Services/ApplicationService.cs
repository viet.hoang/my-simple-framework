using VietHoang.Demo.Domain.Models.Page;
using VietHoang.Demo.Services.Abstractions;
using VietHoang.Demo.Services.Interfaces;
using VietHoang.Orm.Interfaces;

namespace VietHoang.Demo.Services
{
    public class ApplicationService : BaseService, IApplicationService
    {
        public ApplicationService(IGlassContextFactory glassContextFactory)
            : base(glassContextFactory)
        {
        }

        public IStandardPage GetHomePage()
        {
            return ScRequestContext.GetHomeItem<IStandardPage>();
        }
    }
}

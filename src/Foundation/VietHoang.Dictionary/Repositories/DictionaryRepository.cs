﻿using System;
using System.Configuration;
using Glass.Mapper.Sc;
using VietHoang.Core.Models.BaseModels;
using VietHoang.Dictionary.Interfaces;
using VietHoang.Orm.Interfaces;
using Sitecore.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Sites;
using Glass.Mapper.Sc.Web;

namespace VietHoang.Dictionary.Repositories
{
    public class DictionaryRepository : IDictionaryRepository
    {
        private const string MasterDatabaseName = "master";
        private readonly IRequestContext _requestContext;

        public static Models.Dictionary Current
        {
            get
            {
                var glassContextFactory = ServiceLocator.ServiceProvider.GetService<IGlassContextFactory>();
                return new DictionaryRepository(glassContextFactory).Get(SiteContext.Current);
            }
        }

        public DictionaryRepository(IGlassContextFactory glassContextFactory)
        {
            _requestContext = glassContextFactory.GetRequestContext();
        }

        public Models.Dictionary Get(SiteContext site)
        {
            var autoCreate = GetAutoCreateSetting(site);
            var dictionaryRoot = GetDictionaryRoot(site);
            return new Models.Dictionary(site, dictionaryRoot, autoCreate);
        }

        private bool GetAutoCreateSetting(SiteContext site)
        {
            var autoCreateSetting = site.Properties["dictionaryAutoCreate"];
            if (autoCreateSetting == null) return false;

            bool autoCreate;

            if (!bool.TryParse(autoCreateSetting, out autoCreate)) return false;

            return autoCreate && site.Database.Name.Equals(MasterDatabaseName, StringComparison.InvariantCultureIgnoreCase);
        }

        private IStandardItem GetDictionaryRoot(SiteContext site)
        {
            var dictionaryPath = site.Properties["dictionaryPath"];

            if (dictionaryPath == null)
            {
                throw new ConfigurationErrorsException("No dictionaryPath was specified on the <site> definition.");
            }

            var rootItem = _requestContext.SitecoreService.GetItem<IStandardItem>(new GetItemByPathOptions() { Path = dictionaryPath });

            if (rootItem == null)
            {
                throw new ConfigurationErrorsException(
                    "The root item specified in the dictionaryPath on the <site> definition was not found.");
            }

            return rootItem;
        }
    }
}
﻿(function (window) {
    var c, ctx, img, r, th, ir;

    function drawMarker(x, y) {
        ctx.fillStyle = "#c92a2a";
        ctx.beginPath();
        ctx.arc(x, y - th, r, -3 * Math.PI / 2, 3 * Math.PI / 2);
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(x, y);
        ctx.lineTo(x - r * 0.92, y - th * 0.8);
        ctx.lineTo(x + r * 0.92, y - th * 0.8);
        ctx.fill();

        ctx.fillStyle = "#ffffff";
        ctx.beginPath();
        ctx.arc(x, y - th, ir, 0, 2 * Math.PI);
        ctx.fill();
    }

    function mapMarker(canvasId, imageUrl, coorsArray) {

        c = document.getElementById(canvasId);

        this.onClick = null;

        img = new Image();
        img.onload = function () {
            c.width = img.width;
            c.height = img.height;

            ctx = c.getContext("2d");
            ctx.save();

            ctx.clearRect(0, 0, c.width, c.height);
            ctx.drawImage(img, 0, 0, c.width, c.height);

            r = c.width / 100;
            th = r * 2;
            ir = r / 3;
            for (var i = 0; i < coorsArray.length; i++) {
                var coors = coorsArray[i].split(",");
                if (coors.length < 2) continue;
                var initX = parseFloat(coors[0]);
                var initY = parseFloat(coors[1]);
                var x = (initX || 0) * c.width;
                var y = (initY || 0) * c.height;

                drawMarker(x, y);
            }

            ctx.restore();
        };
        img.src = imageUrl;
    }

    window.MapMarker = mapMarker;
})(window);
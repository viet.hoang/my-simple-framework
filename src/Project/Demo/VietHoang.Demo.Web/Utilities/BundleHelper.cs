﻿using System.Web;
using System.Web.Optimization;

namespace VietHoang.Demo.Web.Utilities
{
    public static class BundleHelper
    {
        private const string UniqueBundleId = "0e715699-d79d-4b20-8aba-cfc81ad23792";

        public static void BundleScripts(this BundleCollection bundles, string layoutName, params string[] assets) =>
            bundles.Add(new ScriptBundle(GetScriptBundleName(layoutName)).Include(assets));

        public static void BundleStyles(this BundleCollection bundles, string layoutName, params string[] assets) =>
            bundles.Add(new StyleBundle(GetStyleBundleName(layoutName)).Include(assets));

        public static string GetScriptBundleName(string layoutName) => $"~/bundles/{UniqueBundleId}/{layoutName}/js";

        public static string GetStyleBundleName(string layoutName) => $"~/bundles/{UniqueBundleId}/{layoutName}/css";

        public static IHtmlString ScriptRender(string layoutName) => Scripts.Render(GetScriptBundleName(layoutName));

        public static IHtmlString StyleRender(string layoutName) => Styles.Render(GetStyleBundleName(layoutName));
    }
}

﻿namespace VietHoang.Feature.Language
{
    public struct Templates
    {
        public struct Languages
        {
            public const string Id = "{2E4358B4-8176-4DA0-A965-A52C79C0B3D9}";

            public struct Fields
            {
                public const string SupportedLanguages = "{15D6EBDC-AA5D-4AF1-828B-1BFB8F4A9DA9}";
            }
        }
    }
}

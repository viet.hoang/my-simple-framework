﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;
using VietHoang.Ioc;
using VietHoang.Hoavi.Services;
using VietHoang.Hoavi.Services.Interfaces;
using Sitecore.DependencyInjection;

namespace VietHoang.Hoavi.Web.ServicesConfigurators
{
    public class DependenciesRegistration : IServicesConfigurator
    {
        // AddMvcControllersInCurrentAssembly() method doesn't work in RELEASE mode
        // because of issue with Assembly.GetCallingAssembly()
        // http://www.ticklishtechs.net/2010/03/04/be-careful-when-using-getcallingassembly-and-always-use-the-release-build-for-testing/
        [MethodImpl(MethodImplOptions.NoInlining)]
        public void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IApplicationService, ApplicationService>();

            serviceCollection.AddMvcControllersInCurrentAssembly();
        }
    }
}

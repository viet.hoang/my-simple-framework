**NOTE: if you want to play with Sitecore 8, have a look at [this one](https://gitlab.com/viet.hoang/my-simple-framework/tree/Release-1.9.0)**

For more information, please read the blog posts:
- https://buoctrenmay.com/2017/02/06/a-simple-framework-for-sitecore-xp-8-development/
- https://buoctrenmay.com/2017/04/06/my-simple-sitecore-development-framework-version-1-2/
- https://buoctrenmay.com/2017/04/30/my-simple-sitecore-development-framework-version-1-3/
- https://buoctrenmay.com/2017/06/03/my-simple-sitecore-development-framework-version-1-4/
- https://buoctrenmay.com/2017/07/17/my-simple-sitecore-development-framework-version-1-5/
- https://buoctrenmay.com/2017/08/16/my-simple-sitecore-development-framework-version-1-6/
- https://buoctrenmay.com/2017/11/10/my-simple-sitecore-development-framework-version-1-7/
- https://buoctrenmay.com/2018/02/17/my-simple-sitecore-development-framework-version-1-8/
- https://buoctrenmay.com/2018/05/07/my-simple-sitecore-development-framework-version-1-9-and-auto-deploy-with-teamcity/
- https://buoctrenmay.com/2017/11/14/playing-with-sitecore-9-and-my-simple-framework-version-2-0/
- https://buoctrenmay.com/2018/02/21/sitecore-9-and-my-simple-development-framework-version-2-1/
- https://buoctrenmay.com/2018/08/11/sitecore-9-and-my-simple-development-framework-version-2-2/
- https://buoctrenmay.com/2019/01/03/sitecore-9-and-my-simple-development-framework-version-2-3/
- https://buoctrenmay.com/2019/09/03/sitecore-9-and-my-simple-development-framework-version-2-4/
- https://buoctrenmay.com/2020/01/29/sitecore-9-and-my-simple-development-framework-version-2-5/
- http://buoctrenmay.com/2020/03/25/playing-around-with-sitecore-docker-containers-and-my-simple-development-framework-version-2-6
- https://buoctrenmay.com/2020/10/11/playing-around-with-sitecore-10-docker-containers-and-my-simple-development-framework-version-3-0/
- https://buoctrenmay.com/2021/02/24/playing-around-with-sitecore-10-0-1-docker-containers-and-my-simple-development-framework-version-3-1/
- https://buoctrenmay.com/2021/09/30/playing-around-with-sitecore-10-1-1-docker-containers-and-my-simple-development-framework-version-3-2/

## Main Technologies:
- .Net Framework from version 4.8.0
- Sitecore XP version 10.1.1
- Glass Mapper Sitecore version 5.8: an object mapping framework for Sitecore https://github.com/mikeedwards83/Glass.Mapper
- Unicorn version 4.1.5: a Sitecore utility designed to simplify deployment of Sitecore items across environments automatically https://github.com/kamsar/Unicorn
- Microsoft Visual Studio Unit Testing Framework
- Sitecore.FakeDb version 3.0

## Setup Prerequisites
The following applications/tools/utilities are required on your local machine. The appropriate version of these items should be installed based on the version of your operating system and appropriate licensing.
+ Sitecore XP 10.1.1
+ IIS 10.0+
+ Visual Studio 2017 version 15.7 or later
+ SQL Server 2017 or later
+ IIS URL Rewrite 2.1

## (Optional) Step-by-step Guide for Sitecore Docker container
1. Take a look at [this one](https://containers.doc.sitecore.com/docs/environment-setup) to get your environment set up for container-based Sitecore development
1. rename `\Docker\.env.example` to `\Docker\.env`
1. let's initialize the environment variables
   + launch PowerShell as an administrator
   + change directory to **your Docker folder** (Ex: "D:\\my-simple-framework\\Docker")
   + execute `.\Init -InitEnv` and then input
      + LicenseXmlPath: (Ex: `C:\license\license.xml`)
      + AdminPassword: (Ex: `b`)
   + execute `.\up` to spin up a new **Sitecore 10.1.1** instance
1. rename **cake-config.json.example** to **cake-config.json** and then update the following ones:
   + `WebsiteRoot` to **deploy folder which is mounted to docker container** (Ex: "D:\\\\my-simple-framework\\\\Docker\\\\deploy\\\\website")
   + `InstanceUrl` to **your Sitecore instance url** (Ex: "https://cm.viethoang.local/")
   + `ProjectFolder` to **your solution directory** (Ex: "D:\\\\my-simple-framework")
   + `BuildToolVersions` to **your MS Build version** (Ex: "VS2019")
1. rename **publishsettings.targets.example** to **publishsettings.targets** and then update the following one:
   + `<publishUrl>` to **deploy folder which is mounted to docker container** (Ex: "D:\\my-simple-framework\\Docker\\deploy\\website")
1. rename **Common.LocalSettings.config.example** to **Common.LocalSettings.config**
1. let's deploy the solution to docker container
   + launch PowerShell as an administrator
   + change directory to **your solution folder** (Ex: "D:\\my-simple-framework")
   + execute **.\build -target Deploy-Docker**
1. test your container-based development environment: open a browser, navigate to the following URLs and then you should see them without any errors:
   + [https://hoavi.viethoang.local/](https://hoavi.viethoang.local/)
   + [https://demo.viethoang.local/](https://demo.viethoang.local/)

## (Optional) Step-by-step Guide for vanilla Sitecore instance
1. Take a look at [this one](https://buoctrenmay.com/2021/07/02/sitecore-xp-10-1-update-1-installation-sif-way/) to setup a new **Sitecore 10.1.1** instance whose site name should be `cm.viethoang.local`
   + add the host names `hoavi.viethoang.local`, `demo.viethoang.local` to **hosts** file
   + _Note: you can use [SIM](https://marketplace.sitecore.net/en/Modules/Sitecore_Instance_Manager.aspx) then open `Bundled Tools\Hosts Editor` in order to do that_
1. Site bindings:
   + bind the host names `hoavi.viethoang.local`, `demo.viethoang.local` to your Sitecore site (created in Step 1) via IIS
1. rename **cake-config.json.example** to **cake-config.json** and then update the following ones:
   + `WebsiteRoot` to **your Sitecore webroot folder** (Ex: "D:\\\\websites\\\\cm.viethoang.local")
   + `InstanceUrl` to **your Sitecore instance url** (Ex: "https://cm.viethoang.local/")
   + `ProjectFolder` to **your solution directory** (Ex: "D:\\\\my-simple-framework")
   + `BuildToolVersions` to **your MS Build version** (Ex: "VS2019")
1. rename **publishsettings.targets.example** to **publishsettings.targets** and then update the following one:
   + `<publishUrl>` to **your sitecore website folder** (Ex: `D:\Websites\cm.viethoang.local`)
1. rename **Common.LocalSettings.config.example** to **Common.LocalSettings.config** and then update the following one:
   + `VietHoangSourceFolder` to **your solution directory** (Ex: "D:\\my-simple-framework")
1. Enable URL Rewrite
   + install [IIS URL Rewrite 2.1](https://www.iis.net/downloads/microsoft/url-rewrite)
1. let's deploy the solution:
   + launch PowerShell as an administrator
   + change directory to **your solution folder**
   + execute **.\build**
1. test your local development environment: open a browser, navigate to the following URLs and then you should see them without any errors:
   + [http://hoavi.viethoang.local/](http://hoavi.viethoang.local/)
   + [http://demo.viethoang.local/](http://demo.viethoang.local/)

## Create Deployment Folder
We can perform the build and deloy of a specific environment on the local by following these steps:

1. edit **cake-config.json** by updating the following ones:
	+ `WebsiteRoot` to **your package folder** (Ex: "D:\\\\websites\\\\viethoang.local\\\\Release")
	+ `BuildConfiguration` to appropriate one (Ex: _Release_ or _Release-Staging_)
1. let's deploy the solution:
	+ launch PowerShell as an administrator
	+ change directory to **your solution folder**
	+ execute **.\build -target Quick-Deploy**

## How To Use The Project Templates
1. copy `\project_templates\VietHoang.Template.Web.zip`, `\project_templates\VietHoang.Template.ClassLibrary.zip` files
2. paste them to:
	* **For VS 2015**, paste them to `%USERPROFILE%\Documents\Visual Studio 2015\Templates\ProjectTemplates`
	* **For VS 2017**, paste them to `%USERPROFILE%\Documents\Visual Studio 2017\Templates\ProjectTemplates`
    * **For VS 2019**, paste them to `%USERPROFILE%\Documents\Visual Studio 2017\Templates\ProjectTemplates`

## How To Handle Failed Requests
We can display a friendly generic page for error status code such as **404**, **406**, **500**, etc by adding the XML path `configuration/system.webServer/httpErrors` to `web.config` file:

For instance, if we want to handle request of `hoavi.local/not-found.js` file, we will add the following one:
```
<httpErrors errorMode="Custom">
    <remove statusCode="404" subStatusCode="-1" />
    <error statusCode="404" prefixLanguageFilePath="" path="Hoavi-404.htm" responseMode="File" />
</httpErrors>
```
To support SEO optimization, the status code of `/404Error` page should be **404** so we need to edit `configuration\customErrors` xml node in the **web.config** in order to handle the failed request as well (somehow, we cannot skip IIS custom errors by code logic)
```
<customErrors defaultRedirect="/404Error?" mode="On">
    <error redirect="/404Error?" statusCode="404" />
</customErrors>
```

## Recommendations
Highly recommend that you should install some Visual Studio extensions below as well:

+ [SlowCheetah - XML Transforms](https://marketplace.visualstudio.com/items?itemName=vscps.SlowCheetah-XMLTransforms): it's helpful for creating transform files
+ [File Nesting](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.FileNesting): it's helpful for creating nest files
+ [Markdown Editor](https://marketplace.visualstudio.com/items?itemName=MadsKristensen.MarkdownEditor): a full featured Markdown editor with live preview and syntax highlighting

## Notes
+ use [Cake](https://cakebuild.net/) (C# based) to build and deploy the solution as we can add additional build tasks easily and quickly
+ use [MSBuild .targets file](https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-dot-targets-files) to avoid adding the same file(s) to many projects (it's **VietHoang.Sitecore.targets**)
+ install [RazorGenerator.MsBuild](https://www.nuget.org/packages/RazorGenerator.MsBuild/) Nuget package to Asp.Net MVC projects (normally, they're in _Feature_ and _Project_ layer) so that we will **find out the invalid Razor syntax** soon. It's helpful to avoid deploying the Razor files which would be broken at runtime. The reference: https://kamsar.net/index.php/2016/09/Precompiled-Views-with-Sitecore-8-2/
+ **make the Sitecore solution testable** by using [Sitecore implementation of Dependency Injection](https://doc.sitecore.com/developers/82/sitecore-experience-platform/en/dependency-injection.html). The reference: https://kamsar.net/index.php/2016/08/Dependency-Injection-in-Sitecore-8-2/
+ we CANNOT **Lock and Edit** a Sitecore item if **Unicorn's Transparent Sync** is enabled so please follow these steps to perform integration test on workflow:
	1. open a browser, navigate to `viethoang.local/unicorn.aspx` and then synchronize all the Unicorn's configurations
	1. access `\Website\App_Config\Include\Unicorn\Unicorn.config` and then ensure that `enableTransparentSync="false"`

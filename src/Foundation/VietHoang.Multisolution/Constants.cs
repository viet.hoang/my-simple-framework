﻿namespace VietHoang.Multisolution
{
    public struct Constants
    {
        public struct Cache
        {
            public const string Name = "VietHoang.Multisolution";
            public const string SolutionSitesCacheKey = "VietHoang.Multisolution.SolutionSites";
        }
    }
}
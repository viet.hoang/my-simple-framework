﻿using System.Web;
using System.Web.Optimization;

namespace VietHoang.Hoavi.Web.Utilities
{
    public static class BundleHelper
    {
        private const string UniqueBundleId = "170c91a5-f8f0-459a-9d2f-ef738f2bb392";

        public static void BundleScripts(this BundleCollection bundles, string layoutName, params string[] assets) =>
            bundles.Add(new ScriptBundle(GetScriptBundleName(layoutName)).Include(assets));

        public static void BundleStyles(this BundleCollection bundles, string layoutName, params string[] assets) =>
            bundles.Add(new StyleBundle(GetStyleBundleName(layoutName)).Include(assets));

        public static string GetScriptBundleName(string layoutName) => $"~/bundles/{UniqueBundleId}/{layoutName}/js";

        public static string GetStyleBundleName(string layoutName) => $"~/bundles/{UniqueBundleId}/{layoutName}/css";

        public static IHtmlString ScriptRender(string layoutName) => Scripts.Render(GetScriptBundleName(layoutName));

        public static IHtmlString StyleRender(string layoutName) => Styles.Render(GetStyleBundleName(layoutName));
    }
}

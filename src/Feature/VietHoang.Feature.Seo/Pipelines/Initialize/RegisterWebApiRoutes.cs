﻿using System.Web.Mvc;
using System.Web.Routing;
using Sitecore.Pipelines;

namespace VietHoang.Feature.Seo.Pipelines.Initialize
{
    public class RegisterWebApiRoutes
    {
        public void Process(PipelineArgs args)
        {
            const string solutionName = Shared.Constants.Global.SitecoreSolutionName;

            RouteTable.Routes.MapRoute(
                name: $"{solutionName}.Feature.Seo.Api",
                url: $"api/{solutionName}/Sitemap/{{action}}/{{id}}",
                defaults: new { controller = "Sitemap", id = UrlParameter.Optional },
                namespaces: new[] { $"{solutionName}.Feature.Seo.Controllers" }
            );
        }
    }
}
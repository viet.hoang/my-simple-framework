﻿namespace VietHoang.Sc
{
    public struct Constants
    {
        public struct QueryStringKeys
        {
            public const string Value = "value";
            public const string ContainerId = "containerId";
            public const string Code = "code";
            public const string View = "view";
        }
    }
}
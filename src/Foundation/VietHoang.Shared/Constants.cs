﻿namespace VietHoang.Shared
{
    public struct Constants
    {
        public struct Global
        {
            public const string SitecoreSolutionName = "VietHoang";
        }

        public struct MvcRouteKeys
        {
            public const string Controller = "controller";
        }
    }
}

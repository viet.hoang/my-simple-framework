﻿namespace VietHoang.Demo.Domain
{
    public static class Templates
    {
        public struct StandardPage
        {
            public const string Id = "{CBE45E86-24CA-483C-B4C2-63E6F475EC2D}";
        }

        public struct SimplePage
        {
            public const string Id = "{A6ADC075-2EBD-4EA7-AA4E-D9C077E8F3FE}";
        }

        public struct Map
        {
            public const string Id = "{0005B9B0-E56F-4DF5-B8D0-3F69213488F3}";

            public struct Fields
            {
                public const string MapImage = "{587A1CD5-5840-4602-A97D-1C9FA54AAFE8}";
            }
        }

        public struct Location
        {
            public const string Id = "{9F3105D5-FAFD-46E1-B956-D4A3AF73F607}";

            public struct Fields
            {
                public const string LocationName = "{DE461B2E-0774-4245-95A9-5FABF5586819}";
                public const string LocationCoordinates  = "{70010238-3454-4B4A-8093-DC175EF990C4}";
            }
        }

        public struct SiteRoot
        {
            public const string Id = "{065D8FE1-B263-4FC3-9059-C6BA6934C3D7}";
        }
    }
}

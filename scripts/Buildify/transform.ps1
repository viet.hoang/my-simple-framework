Param(
	[string] $psTransform = "transform.ps1",
	[string] $sourcePath = "D:\my-simple-framework",
	[string] $currentIdentifier = "VietHoang",
	[string] $newIdentifier = "YourClientName"
)

function TransformNameOfFoldersAndFiles($path = $sourcePath, [string[]]$exclude = @($psTransform)) 
{ 
	foreach ($item in Get-ChildItem $path)
	{
		if ($exclude | Where {$item.Name -like $_}) { continue }

		
		if (Test-Path $item.FullName -PathType Container) 
		{
			$item
			TransformNameOfFoldersAndFiles $item.FullName $exclude
			
			if ($item.Name -like "*$($currentIdentifier)*") 
			{
				Write-Host "TRANSFORMING NAME OF FOLDER: " $item.Fullname 
				$newName = $item.Name -replace $currentIdentifier, $newIdentifier
				Write-Host "NEW NAME " $newName
				Rename-Item -Path $item.Fullname -NewName $newName
			}
		} 
		else 
		{ 
			Write-Host "TRANSFORMING NAME OF FILE: " $item.Fullname
			$item | Rename-Item -NewName {$_.Name -replace $currentIdentifier, $newIdentifier}
			$item
		}
	}
}

function TransformContentOfFiles($path = $sourcePath, [string[]]$exclude = @($psTransform)) 
{ 
	foreach ($item in Get-ChildItem $path)
	{
		if ($exclude | Where {$item.Name -like $_}) { continue }

		
		if (Test-Path $item.FullName -PathType Container) 
		{
			$item
			TransformContentOfFiles $item.FullName $exclude
		} 
		else 
		{ 
			Write-Host "TRANSFORMING CONTENT OF FILE: " $item.Fullname
			$content = Get-Content $item.Fullname -Raw
			$newContent = ($content -replace $currentIdentifier, $newIdentifier)
			# Note: set-content adds newlines by default
			Set-Content $item.Fullname -Value $newContent -NoNewLine
			$item
		}
	}
}

TransformNameOfFoldersAndFiles
TransformContentOfFiles
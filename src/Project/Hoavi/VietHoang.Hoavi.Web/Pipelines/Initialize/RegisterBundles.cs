﻿using System.Web.Optimization;
using Sitecore.Pipelines;
using VietHoang.Hoavi.Web.Utilities;

namespace VietHoang.Hoavi.Web.Pipelines.Initialize
{
    public class RegisterBundles
    {
        public void Process(PipelineArgs args)
        {
            Register(BundleTable.Bundles);
        }

        private static void Register(BundleCollection bundles)
        {
            RegisterLayout(bundles);
            RegisterOfficesMap(bundles);
        }

        private static void RegisterLayout(BundleCollection bundles)
        {
            bundles.BundleStyles(Constants.Layouts.Layout,
                "~/include/css/homepage.css");

            bundles.BundleScripts(Constants.Layouts.Layout,
                "~/include/js/jquery.lazy.js",
                "~/include/js/jquery.transit.min.js",
                "~/include/js/homepage.js");
        }

        private static void RegisterOfficesMap(BundleCollection bundles)
        {
            bundles.BundleScripts(Constants.Layouts.OfficesMap,
                $"~/assets/Hoavi/js/jquery-2.1.0.min.js",
                $"~/assets/Hoavi/js/marker.js");
        }
    }
}
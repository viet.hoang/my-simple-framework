﻿using System;
using System.Collections;
using Sitecore.Caching;
using Sitecore.Diagnostics;

namespace VietHoang.Common.Web.Events.PublishEnd
{
    public class CacheClearer
    {
        public ArrayList Caches { get; } = new ArrayList();

        public void ClearCaches(object sender, EventArgs args)
        {
            Assert.ArgumentNotNull(sender, "sender");
            Assert.ArgumentNotNull(args, "args");
            try
            {
                foreach (string cacheName in this.Caches)
                {
                    var cache = CacheManager.FindCacheByName<CustomCache>(cacheName);
                    cache?.Clear();
                }
            }
            catch (Exception ex)
            {
                Log.Error($"{this}:{ex}", ex, this);
            }
        }
    }
}
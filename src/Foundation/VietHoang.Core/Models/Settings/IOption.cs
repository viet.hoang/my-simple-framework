﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Core.Models.Settings
{
    [SitecoreType(TemplateId = Templates.Option.Id, AutoMap = true)]
    public interface IOption : IStandardItem
    {
        [SitecoreField(Templates.Option.Fields.Value)]
        string Value { get; set; }
    }
}
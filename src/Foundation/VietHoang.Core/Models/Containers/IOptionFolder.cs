﻿using Glass.Mapper.Sc.Configuration.Attributes;
using VietHoang.Core.Models.BaseModels;

namespace VietHoang.Core.Models.Containers
{
    [SitecoreType(TemplateId = Templates.OptionFolder.Id, AutoMap = true)]
    public interface IOptionFolder : IStandardItem, INeedsChildren
    { 
    }
}